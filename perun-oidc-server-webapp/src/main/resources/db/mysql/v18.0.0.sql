CREATE TABLE IF NOT EXISTS client_only_allowed_idps (
    owner_id BIGINT,
    idp_entity_id VARCHAR(512)
);

CREATE TABLE IF NOT EXISTS client_blocked_idps (
    owner_id BIGINT,
    idp_entity_id VARCHAR(512)
);

alter table client_only_allowed_idps
    add constraint client_only_allowed_idps_client_details_id_fk
        foreign key (owner_id) references client_details (id)
            on update cascade on delete cascade;

alter table client_blocked_idps
    add constraint client_blocked_idps_client_details_id_fk
        foreign key (owner_id) references client_details (id)
            on update cascade on delete cascade;