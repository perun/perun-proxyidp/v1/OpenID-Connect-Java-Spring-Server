const selectGroup = $(".selectGroup");

function filter() {
  hideGroups();
  selectGroup.val("");
  const vo = $("#selectVo").val();
  if (vo !== "") {
    showGroups();
    $(".groupOption").each(function () {
      const value = $(this).val();
      if (value.startsWith(vo, 0)) {
        $(this).show();
      } else {
        $(this).hide();
      }
    });
  }
}
function showGroups() {
  selectGroup.show();
  selectGroup.prop("required", true);
  selectGroup.prop("disabled", false);
}
function hideGroups() {
  selectGroup.hide();
  selectGroup.prop("required", false);
  selectGroup.prop("disabled", true);
}
$(document).ready(function () {
  $("#selectVo").val("");
});
