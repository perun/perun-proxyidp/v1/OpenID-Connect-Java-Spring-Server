<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags/common" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<c:set var="reqURL" value="${reqURL}"/>


<%

	String samlCssUrl = (String) request.getAttribute("samlResourcesURL");
	List<String> cssLinks = new ArrayList<>();

	cssLinks.add(samlCssUrl + "/module.php/consent/assets/css/consent.css");
	cssLinks.add(samlCssUrl + "/module.php/perun/res/css/consent.css");

	request.setAttribute("cssLinks", cssLinks);

%>

<spring:message code="device_approve_title" var="title"/>
<t:header title="${title}" reqURL="${reqURL}" baseURL="${baseURL}"
          cssLinks="${cssLinks}" theme="${theme}" samlResourcesURL="${samlResourcesURL}"/>

<h1 class="h3"><spring:message code="device_approve_header"/> ${" "} ${fn:escapeXml(client.clientName)}</h1>

</div> <%-- header --%>

<div id="content">
    <c:remove scope="session" var="SPRING_SECURITY_LAST_EXCEPTION" />
    <c:if test="${getsOfflineAccess}">
        <div class="alert alert-warning text-justify" role="alert">
            <h4>Continuous data access.</h4>
            <p>This service requests continuous access to your data. That means that the service might continuously fetch the
                information you allow to be released via the form below without further interaction needed.</p>
        </div>
    </c:if>
    <c:if test="${not empty(jurisdiction)}">
        <div class="alert alert-warning text-justify" role="alert">
            <c:choose>
                <c:when test="${'EMBL'.equalsIgnoreCase(jurisdiction)} or ${'INT'.equalsIgnoreCase(jurisdiction)}">
                    <h4>This service is provided by an international organization.</h4>
                </c:when>
                <c:otherwise>
                    <h4>This service is in ${fullJurisdiction}</h4>
                </c:otherwise>
            </c:choose>
            <c:choose>
                <c:when test="${'EMBL'.equalsIgnoreCase(jurisdiction)}">
                    <p>In order to access the requested services, the Life Science Login needs to transfer your personal data to
                        an international organization outside EU/EEA jurisdictions.</p>
                    <p>Please be aware that upon transfer your personal data will be protected by
                        <a href="https://www.embl.org/documents/document/internal-policy-no-68-on-general-data-protection/"
                           target="_blank">EMBL’s Internal Policy 68 on General Data Protection</a>.</p>;
                </c:when>
                <c:otherwise>
                    <p>In order to access the requested services, the Life Science Login needs to transfer your personal data to
                        a country outside EU/EEA. We cannot guarantee that this country offers an adequately high level of personal
                        data protection as EU/EEA countries.</p>
                </c:otherwise>
            </c:choose>
        </div>
    </c:if>
    <c:if test="${not acceptedTos}">
        <div class="alert alert-warning" role="alert">
            <h4>Terms of Use for Service Providers not accepted</h4>
            <p class="text-justify">You are entering a service which has not yet accepted the
                <a href="https://lifescience-ri.eu/ls-login/terms-of-use-for-service-providers.html"
                   target="_blank">Terms of Use for Service Providers</a>.
                <c:if test="${isTestSp}">
                    This might be due to the service being registered in the test environment, which does not force the service to
                    do so. To get more information about the different environments of the LS Login, please visit
                    <a href="https://lifescience-ri.eu/ls-login/relying-parties/environments.html" target="_blank">this page</a>.
                </c:if>
            </p>
        </div>
    </c:if>
        <form name="confirmationForm"
              action="${ config.issuer }${ config.issuer.endsWith('/') ? '' : '/' }auth/device/approved" method="post">
            <p>
                <c:if test="${not empty client.policyUri}">
                    <spring:message code="device_approve_privacy"/>${" "}<a target='_blank' href='${fn:escapeXml(client.policyUri)}'><em>${fn:escapeXml(client.clientName)}</em></a>
                </c:if>
            </p>
            <t:attributesConsent/>
            <input id="user_oauth_approval" name="user_oauth_approval" value="true" type="hidden" />
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
            <input type="hidden" name="user_code" value="${ dc.userCode }" />
            <t:consentButtons/>
        </form>
    </div>
</div><!-- wrap -->

<t:footer baseURL="${baseURL}" theme="${theme}" samlResourcesURL="${samlResourcesURL}"/>
