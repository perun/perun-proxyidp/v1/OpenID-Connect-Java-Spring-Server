<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" trimDirectiveWhitespaces="true" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="cz.muni.ics.oidc.web.controllers.PerunUnauthorizedController" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags/common"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%

String samlCssUrl = (String) request.getAttribute("samlResourcesURL");
List<String> cssLinks = new ArrayList<>();

cssLinks.add(samlCssUrl + "/module.php/perun/res/css/perun_identity_unauthorized_register_notify_action_required.css");

request.setAttribute("cssLinks", cssLinks);

%>

<spring:message code="unauthorized_register_notify_action_required_title" var="title"/>
<t:header title="${title}" reqURL="${reqURL}" baseURL="${baseURL}"
          cssLinks="${cssLinks}" theme="${theme}" samlResourcesURL="${samlResourcesURL}"/>

</div> <%-- header --%>

<div id="content">
    <div id="head">
        <h1><spring:message code="unauthorized_register_notify_action_required_header1"/>
            <c:choose>
                <c:when test="${not empty client.clientName and not empty client.clientUri}">
                    ${" "}<a href="${fn:escapeXml(client.clientUri)}" target="_blank">${fn:escapeXml(client.clientName)}</a>
                </c:when>
                <c:when test="${not empty client.clientName}">
                    ${" "}${fn:escapeXml(client.clientName)}
                </c:when>
            </c:choose>
            ${" "}<spring:message code="unauthorized_register_notify_action_required_header2"/>
        </h1>
    </div>
    <form action="${pageContext.request.contextPath}${PerunUnauthorizedController.UNAUTHORIZED_REGISTER_CHOOSE_VO_GROUP_MAPPING}" method="GET">
        <hr/>
        <br/>
        <spring:message code="unauthorized_register_notify_action_required_continue" var="submit_value"/>
        <input type="submit" name="continueToRegistration" value="${submit_value}"
               class="btn btn-lg btn-primary btn-block">
    </form>
</div>
</div><!-- ENDWRAP -->

<t:footer baseURL="${baseURL}" theme="${theme}" samlResourcesURL="${samlResourcesURL}"/>