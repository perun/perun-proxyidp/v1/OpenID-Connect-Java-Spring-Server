<%@ page contentType="text/html; charset=utf-8" pageEncoding="utf-8" trimDirectiveWhitespaces="true" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags/common"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%

List<String> cssLinks = new ArrayList<>();

request.setAttribute("cssLinks", cssLinks);

%>

<t:header title="${title}" reqURL="${reqURL}" baseURL="${baseURL}"
          cssLinks="${cssLinks}" theme="${theme}" samlResourcesURL="${samlResourcesURL}"/>

</div> <%-- header --%>

<div id="content">
    <div class="error_message" style="word-wrap: break-word;">
        <h1><spring:message code="${outHeader}"/></h1>
        <p><spring:message code="${outMessage}"/></p>
        <c:if test="${not empty target}">
            <form method="GET" action="${target}" class="mb-4">
                <input class="btn btn-primary btn-block" value="<spring:message code="${outButton}"/>" type="submit"/>
            </form>
        </c:if>
        <c:if test="${not empty client and not empty client.contacts}">
            <p><spring:message code="403_is_eligible_client_contact"/></p>
            <ul>
                <c:forEach items="${client.contacts}" var="contact">
                    <li>
                        <a href="mailto:${fn:escapeXml(contact)}">${fn:escapeXml(contact)}</a>
                    </li>
                </c:forEach>
            </ul>
        </c:if>
        <p>
            <spring:message code="${outContactP}"/>${" "}
            <a href="mailto:${fn:escapeXml(contactMail)}">${fn:escapeXml(contactMail)}</a>
        </p>
    </div>
</div>
</div><!-- ENDWRAP -->

<t:footer baseURL="${baseURL}" theme="${theme}" samlResourcesURL="${samlResourcesURL}"/>
