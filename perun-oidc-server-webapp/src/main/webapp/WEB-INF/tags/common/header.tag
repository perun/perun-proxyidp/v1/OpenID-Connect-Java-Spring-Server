<%@ tag pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="o" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="cesnet" tagdir="/WEB-INF/tags/cesnet" %>
<%@ taglib prefix="einfra" tagdir="/WEB-INF/tags/einfra" %>
<%@ taglib prefix="ceitec" tagdir="/WEB-INF/tags/ceitec" %>
<%@ taglib prefix="envri" tagdir="/WEB-INF/tags/envri" %>
<%@ taglib prefix="elter" tagdir="/WEB-INF/tags/elter" %>
<%@ taglib prefix="muni" tagdir="/WEB-INF/tags/muni" %>
<%@ taglib prefix="lsaai" tagdir="/WEB-INF/tags/lsaai" %>
<%@ attribute name="title" required="true" %>
<%@ attribute name="reqURL" required="true" %>
<%@ attribute name="baseURL" required="true" %>
<%@ attribute name="samlResourcesURL" required="true" %>
<%@ attribute name="theme" required="true" %>
<%@ attribute name="cssLinks" required="true" type="java.util.ArrayList<java.lang.String>" %>

<c:choose>
    <c:when test="${theme eq 'cesnet'}">
        <cesnet:header title="${title}" reqURL="${reqURL}" cssLinks="${cssLinks}" baseURL="${baseURL}" samlResourcesURL="${samlResourcesURL}"/>
    </c:when>
    <c:when test="${theme eq 'einfra'}">
        <einfra:header title="${title}" reqURL="${reqURL}" cssLinks="${cssLinks}" baseURL="${baseURL}" samlResourcesURL="${samlResourcesURL}"/>
    </c:when>
    <c:when test="${theme eq 'ceitec'}">
        <ceitec:header title="${title}" reqURL="${reqURL}" cssLinks="${cssLinks}" baseURL="${baseURL}" samlResourcesURL="${samlResourcesURL}"/>
    </c:when>
    <c:when test="${theme eq 'envri'}">
        <envri:header title="${title}" reqURL="${reqURL}" cssLinks="${cssLinks}" baseURL="${baseURL}" samlResourcesURL="${samlResourcesURL}"/>
    </c:when>
    <c:when test="${theme eq 'elter'}">
        <elter:header title="${title}" reqURL="${reqURL}" cssLinks="${cssLinks}" baseURL="${baseURL}" samlResourcesURL="${samlResourcesURL}"/>
    </c:when>
    <c:when test="${theme eq 'muni'}">
        <muni:header title="${title}" reqURL="${reqURL}" cssLinks="${cssLinks}" baseURL="${baseURL}" samlResourcesURL="${samlResourcesURL}"/>
    </c:when>
    <c:when test="${theme eq 'lsaai'}">
        <lsaai:header title="${title}" reqURL="${reqURL}" cssLinks="${cssLinks}" baseURL="${baseURL}" samlResourcesURL="${samlResourcesURL}"/>
    </c:when>
    <c:otherwise>
        <o:header title="${title}"/>
    </c:otherwise>
</c:choose>
