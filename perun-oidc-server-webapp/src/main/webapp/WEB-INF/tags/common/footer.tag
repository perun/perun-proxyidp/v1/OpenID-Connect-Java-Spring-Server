<%@ tag pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="o" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="cesnet" tagdir="/WEB-INF/tags/cesnet" %>
<%@ taglib prefix="einfra" tagdir="/WEB-INF/tags/einfra" %>
<%@ taglib prefix="ceitec" tagdir="/WEB-INF/tags/ceitec" %>
<%@ taglib prefix="envri" tagdir="/WEB-INF/tags/envri" %>
<%@ taglib prefix="elter" tagdir="/WEB-INF/tags/elter" %>
<%@ taglib prefix="muni" tagdir="/WEB-INF/tags/muni" %>
<%@ taglib prefix="lsaai" tagdir="/WEB-INF/tags/lsaai" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags/common" %>
<%@ attribute name="baseURL" required="true" %>
<%@ attribute name="samlResourcesURL" required="true" %>
<%@ attribute name="theme" required="true" %>

<c:choose>
    <c:when test="${theme eq 'cesnet'}">
        <cesnet:footer baseURL="${baseURL}" samlResourcesURL="${samlResourcesURL}"/>
    </c:when>
    <c:when test="${theme eq 'einfra'}">
        <einfra:footer baseURL="${baseURL}" samlResourcesURL="${samlResourcesURL}"/>
    </c:when>
    <c:when test="${theme eq 'ceitec'}">
        <ceitec:footer baseURL="${baseURL}" samlResourcesURL="${samlResourcesURL}"/>
    </c:when>
    <c:when test="${theme eq 'envri'}">
        <envri:footer baseURL="${baseURL}" samlResourcesURL="${samlResourcesURL}"/>
    </c:when>
    <c:when test="${theme eq 'elter'}">
        <elter:footer baseURL="${baseURL}" samlResourcesURL="${samlResourcesURL}"/>
    </c:when>
    <c:when test="${theme eq 'muni'}">
        <muni:footer/>
    </c:when>
    <c:when test="${theme eq 'lsaai'}">
        <lsaai:footer baseURL="${baseURL}" samlResourcesURL="${samlResourcesURL}"/>
    </c:when>
    <c:otherwise>
        <o:footer />
    </c:otherwise>
</c:choose>
<script type="text/javascript" src="${baseURL}/resources/js/jquery-3-3-1.min.js"></script>

</body>
</html>
