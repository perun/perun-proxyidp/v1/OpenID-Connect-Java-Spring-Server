<%@ tag pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ attribute name="js" required="false"%>
<%@ attribute name="baseURL" required="true"%>
<%@ attribute name="samlResourcesURL" required="true"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags/common" %>
<jsp:useBean id="date" class="java.util.Date" />

<div style="text-align: center;">
    <div id="footer" style="display: flex; justify-content: space-between; margin: 0 auto; max-width: 1000px;">
        <div>
            <img src="${samlResourcesURL}/module.php/perun/res/img/elter_logo_120.png" alt=" Logo">
        </div>
        <div>
            <img src="${samlResourcesURL}/module.php/perun/res/img/eu_logo_120.png" alt="EU Logo">
        </div>
        <div>
            <p>eLTER receives funding from the European Union’s Horizon 2020 research and innovation programme under GA No 871126 (eLTER PPP) and GA No 871128 (eLTER PLUS), and the European Union’s Horizon Europe research and innovation programme under GA No 101131751 (eLTER EnRich).</p>
        </div>
    </div>
</div>
