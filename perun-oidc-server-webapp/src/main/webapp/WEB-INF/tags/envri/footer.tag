<%@ tag pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ attribute name="js" required="false"%>
<%@ attribute name="baseURL" required="true"%>
<%@ attribute name="samlResourcesURL" required="true"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags/common" %>
<jsp:useBean id="date" class="java.util.Date" />

<div style="text-align: center;">
    <div id="footer" style="display: flex; justify-content: space-between; margin: 0 auto; max-width: 1000px;">
        <div>
            <img src="${samlResourcesURL}/module.php/perun/res/img/envri_logo_120.png" alt="ENVRI Logo">
        </div>
        <div>
            <img src="${samlResourcesURL}/module.php/perun/res/img/eu_logo_120.png" alt="EU Logo">
        </div>
        <div>
            <p>Virtual ENVRI community platform is maintained thanks to ENVRI-FAIR project.
                The project received funding from the European Union’s Horizon 2020 research and innovation
                programme under grant agreement No 824068.</p>
            <p>ENVRI-FAIR is coordinated by Forschungszentrum Jülich.</p>
            <p><a href="mailto:elter@ics.muni.cz">elter@ics.muni.cz</a></p>
            <p>Copyright © ENVRI-FAIR <?php echo date("Y"); ?></p>
        </div>
    </div>
</div>
