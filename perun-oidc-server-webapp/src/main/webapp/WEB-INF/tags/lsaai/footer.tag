<%@ tag pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ attribute name="js" required="false"%>
<%@ attribute name="baseURL" required="true"%>
<%@ attribute name="samlResourcesURL" required="true"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags/common" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<jsp:useBean id="date" class="java.util.Date" />

<div class="container" id="footer">
    <div class="row mt-1">
        <div class="col-xs-3 col-md-2">
            <div class="img-wrap" id="footer-ls-logo">
                <a href="https://lifescience-ri.eu/ls-login/" target="_blank">
                    <img src="${samlResourcesURL}/module.php/perun/res/img/lsaai_logo_120.png"
                         alt="European Life Science Research Infrastructures Logo">
                </a>
            </div>
        </div>
        <div class="col-xs-9 col-md-10 mt-xs-1">
            <p class="text-justify ">LS Login, an authentication service of the European Life Science Research
                Infrastructures (LS RI), is a community platform established via the EOSC-Life project and operated by
                Masaryk University, Brno, CZ. Visit our
                <a href="https://lifescience-ri.eu/ls-login/" target="_blank">homepage</a>
                or contact us at
                <a href="mailto:support@aai.lifescience-ri.eu">support@aai.lifescience-ri.eu</a>.</p>
        </div>
    </div>
    <div class="row mb-2 mt-1">
        <div class="col-xs-3 col-md-2">
            <div class="img-wrap">
                <img src="${samlResourcesURL}/module.php/perun/res/img/eu_logo_120.png"
                     alt="European Union flag">
            </div>
        </div>
        <div class="col-xs-9 col-md-10 mt-xs-1">
            <p class="text-justify">
                <a href="https://lifescience-ri.eu/" target="_blank">The European Life Science Research
                    Infrastructures</a> has received funding from the European Union’s Horizon 2020 research
                and innovation programme under grant agreement No 654248 and from the European Union’s Horizon
                2020 programme under grant agreement number 824087.
            </p>
        </div>
    </div>
</div>
