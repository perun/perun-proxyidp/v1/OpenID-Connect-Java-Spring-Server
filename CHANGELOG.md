## [18.7.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v18.7.1...v18.7.2) (2025-03-06)


### Bug Fixes

* release oidc.war path 404 error ([74d71ff](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/74d71ffd795b66395495c3499207ae74076c24fd))

## [18.7.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v18.7.0...v18.7.1) (2025-01-11)


### Bug Fixes

* change ico in elter theme ([920a17e](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/920a17e163cc16cdc573f13c56e9f648ac800665))

# [18.7.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v18.6.1...v18.7.0) (2024-12-29)


### Features

* add elterh theme ([30d70fc](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/30d70fc469a9812fd3262401e33b0857fee7e7a5))

## [18.6.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v18.6.0...v18.6.1) (2024-07-18)


### Bug Fixes

* 🐛 LS AAI header CSS ([5cdb87f](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/5cdb87fa8e0b8761744973c140fe4afb32bca537))

# [18.6.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v18.5.5...v18.6.0) (2024-07-17)


### Features

* set x-frame-options to sameorigin instead to deny ([f1da552](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/f1da5522abc004f4608b951459fdb7ec89d72aa7))

## [18.5.5](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v18.5.4...v18.5.5) (2024-07-16)


### Bug Fixes

* 🐛 More clear rext for regs into groups and VOS ([4393828](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/4393828cfd848b3b5b187e226f701b2b09fe5812))

## [18.5.4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v18.5.3...v18.5.4) (2024-06-19)


### Bug Fixes

* 🐛 Typo in device_code approve translation ([4920958](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/4920958dc7b1a9f9b2ff5b0ed3d72e2715cc5cb6))

## [18.5.3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v18.5.2...v18.5.3) (2024-05-30)


### Bug Fixes

* 🐛 Losing AUD in GA4GH AT modifier ([a4656c6](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/a4656c6757ee06ab75f9a1185520f208dd158637))

## [18.5.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v18.5.1...v18.5.2) (2024-05-17)


### Bug Fixes

* 🐛 Entitlements sources - do not ignore res. capabilities ([f14c85e](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/f14c85e9d46f4e3227237064070b18750f8c0e2b))

## [18.5.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v18.5.0...v18.5.1) (2024-05-15)


### Bug Fixes

* 🐛 claim modifiers in arrays retain only unique values ([5d2750d](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/5d2750d55c0acf3f7fb76f4b549de5e45d789300))

# [18.5.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v18.4.0...v18.5.0) (2024-05-15)


### Features

* 🎸 filtering of assigned resource groups in entitlements ([7f1cf4c](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/7f1cf4c531fe5b7dd5069bcb4645f35dbc8662f4))
* 🎸 filtering of groups in access control filter ([76ce690](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/76ce690363dbe25097836f609ab6f6aca2197b5b))

# [18.4.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v18.3.1...v18.4.0) (2024-04-23)


### Bug Fixes

* 🐛 add console log appender for docker pruposes ([709abd4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/709abd4ffbfbf7af547c75f3d14df930d5a25bf3))


### Features

* 🎸 VoBasedEdupersonScopedAffiliationsClaimSource impl ([adae854](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/adae854208e7dab2aa9600e395b2c58edb48df37))

## [18.3.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v18.3.0...v18.3.1) (2024-04-10)


### Bug Fixes

* empty release ([ba49134](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/ba49134e5b15bb9b99ba77cf08dd987c783ab15a))

# [18.3.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v18.2.0...v18.3.0) (2024-04-09)


### Features

* 🎸 allow claimModifier to replace old value (configurable) ([c615346](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/c61534601a7ec94325d47b2f07fa3eeaa0ff5bdf))

# [18.2.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v18.1.0...v18.2.0) (2024-04-08)


### Features

* 🎸 Add ERIC jurisdiction as EU_EAA ([42c7884](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/42c788489ea8e800cfe7cb687771565df6ad1355))

# [18.1.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v18.0.2...v18.1.0) (2024-04-03)


### Features

* 🎸 Pretty print GA4GH in consent ([a249add](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/a249addd5a19040f748b13aba8c4e8608b7dc8ec))

## [18.0.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v18.0.1...v18.0.2) (2024-03-25)


### Bug Fixes

* 🐛 Jurisdiction in LS AAI consents ([b000b8e](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/b000b8e43a350d27cdef61768a68ac6a21e451b0))

## [18.0.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v18.0.0...v18.0.1) (2024-03-25)


### Bug Fixes

* 🐛 Fix jurisdiction in consent ([b2514df](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/b2514dfbf5677824e8ef1fcc8ace4459a12f3211))

# [18.0.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v17.3.3...v18.0.0) (2024-03-19)


### Bug Fixes

* 🐛 update LS AAI theme ([702c58c](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/702c58cef7f3d7fbf6e65483145e18b623bf21e9))


### Features

* 🎸 Pass in ACRs onlyAllowed and blocked IdPs ([ec6662b](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/ec6662b6c9f7865bc849d559c926e68ce86fe507))


### BREAKING CHANGES

* requires database update (see v18.0.0.sql script)

## [17.3.3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v17.3.2...v17.3.3) (2024-03-18)


### Bug Fixes

* 🐛 Throw correct error when no auth_code found ([1105142](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/1105142f6be056ec6b432a307641f1155c81253f))

## [17.3.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v17.3.1...v17.3.2) (2024-03-18)


### Bug Fixes

* 🐛 Improve request logging format ([a493770](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/a493770ff976938bdb33556b2aa5861364aff583))

## [17.3.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v17.3.0...v17.3.1) (2024-03-13)


### Bug Fixes

* 🐛 Add switch for LogRequests filter ([a2d190a](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/a2d190ae19789b554a55f2f770e790dcfe658ff1))

# [17.3.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v17.2.2...v17.3.0) (2024-03-13)


### Features

* 🎸 Log incomming requests ([b28c941](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/b28c941d6a155143b11e615b39eba5a8a5b06caa))

## [17.2.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v17.2.1...v17.2.2) (2024-03-13)


### Bug Fixes

* 🐛 Fix cleanup savedUserAuth ([5d559b2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/5d559b2da70b07fce7343918536d8ae6d054bd13))
* 🐛 Update DB scripts to contain all modifications ([64e9f37](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/64e9f377cbfc0815e4ee57a1f0ecc54db7f6aa4f))

## [17.2.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v17.2.0...v17.2.1) (2024-03-01)


### Bug Fixes

* update link in copyright ([86a66c7](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/86a66c706dd55c08527173133486cde3ecceb0b0))

# [17.2.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v17.1.5...v17.2.0) (2024-02-28)


### Features

* 🎸 Dynreg - delete client ([73c7076](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/73c70764701589155cd68c80d2b07300e7d9f0d7))

## [17.1.5](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v17.1.4...v17.1.5) (2024-02-28)


### Bug Fixes

* 🐛 remove aud extension from token extra info ([e73a18d](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/e73a18d92e2ed8216beaa0306c92e4f0834faed8))

## [17.1.4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v17.1.3...v17.1.4) (2024-02-21)


### Bug Fixes

* 🐛 scheduled tasks, missing constant and non-used param ([61ab061](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/61ab061cb87b0e4780b7f1b5ddcc40a82cbfe43a))

## [17.1.3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v17.1.2...v17.1.3) (2024-02-14)


### Bug Fixes

* 🐛 default values for dynreg ([93f2a85](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/93f2a85b46df5419eb20286bc69cb633ec471c47))
* 🐛 remove addit. info. (aud, resource) from token responses ([0e0996d](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/0e0996d228afdb788e66e6fe35b5338db9caac37))

## [17.1.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v17.1.1...v17.1.2) (2024-02-14)


### Bug Fixes

* 🐛 fix refresh auds for tokens via token exchange granter ([06053a3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/06053a3bf7f289c4fbc9c343fccc8ce7b077b222))

## [17.1.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v17.1.0...v17.1.1) (2024-02-09)


### Bug Fixes

* 🐛 Set correct audience for refreshed access_token ([5465285](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/5465285259ea01e8516836336be3240b529c0b35))

# [17.1.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v17.0.5...v17.1.0) (2024-02-05)


### Bug Fixes

* 🐛 dynreg resource claim and allowed resource comparation ([21ceb47](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/21ceb47e21a1a10524e7410ef77e038b06ebc87a))
* 🐛 use def. scopes if no scope param is present in tok exch ([9eef74b](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/9eef74b41250f7134561b38d52bed0444bbecb58))


### Features

* envri theme ([36dbfe5](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/36dbfe507aeb92d1d12108be930421c96aa01d72))

## [17.0.5](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v17.0.4...v17.0.5) (2024-01-23)


### Bug Fixes

* 🐛 parsing body in dynreg, JSON keys need to be in snakecas ([9fe3b3b](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/9fe3b3bd9472827fdd9c80d8168da3945a40583d))

## [17.0.4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v17.0.3...v17.0.4) (2024-01-22)


### Bug Fixes

* 🐛 requested parameters in dynreg ([1c18b72](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/1c18b72cea92de372b43a7ed574b9d5705fd2f11))

## [17.0.3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v17.0.2...v17.0.3) (2024-01-19)


### Bug Fixes

* 🐛 Enable refresh for ClientCredentials grant ([05d5068](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/05d5068c04bb904afa06bc36544bc63959744967))
* 🐛 Fix resourceIds comparation ([430a84e](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/430a84e60494c471e11e6a5a7ba121be5c646349))

## [17.0.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v17.0.1...v17.0.2) (2024-01-15)


### Bug Fixes

* 🐛 Fix DB conversion of PKCE from DB data (wrong lookup ([05c5d30](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/05c5d30fd228c953b99468ed240640649b2a6e3a))

## [17.0.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v17.0.0...v17.0.1) (2024-01-12)


### Bug Fixes

* 🐛 Fix loading of non-existing controller ([3ccb517](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/3ccb517604ac0709aba6add359f0daf9275a5691))

# [17.0.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v16.1.3...v17.0.0) (2023-12-27)


### Features

* 🎸 Implementation of dynamic registration ([566e419](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/566e41908548cbdaaa284c1ec9b42e870edb035e))


### BREAKING CHANGES

* Requires database update v17.0.0.sql

## [16.1.3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v16.1.2...v16.1.3) (2023-12-22)


### Bug Fixes

* 🐛 discovery endpoint data ([9906c78](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/9906c785ce12a1de37b10a852cde890bb847a1db))

## [16.1.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v16.1.1...v16.1.2) (2023-11-22)


### Bug Fixes

* 🐛 Fixed in regForm and regFormContinue ([133c29e](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/133c29e29759af7016971236fae6aa9bd25706e2))

## [16.1.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v16.1.0...v16.1.1) (2023-11-15)


### Bug Fixes

* 🐛 Fix client_credentials grant resource_ids parameter ([59b4b54](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/59b4b546c54b8b0d323ddfadb02206689651ebb4))

# [16.1.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v16.0.0...v16.1.0) (2023-11-14)


### Features

* 🎸 Enable client-crednetials grant type ([0ccfb28](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/0ccfb2839432d6fee262eb8d4bff758e483da424))

# [16.0.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v15.6.0...v16.0.0) (2023-09-17)


### Bug Fixes

* 🐛 Remove BBMRI theme ([f58d04d](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/f58d04db533cbe086b2027cf60e06efb3a59d670))
* 🐛 Use local resources for LS AAI theme ([2bcc6f5](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/2bcc6f55cebc6b231f0902c3a89629141b0718e9))


### BREAKING CHANGES

* 🧨 BBMRI theme is no longer available

# [15.6.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v15.5.0...v15.6.0) (2023-07-19)


### Features

* 🎸 Support for resources parameter ([2b0e720](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/2b0e7204b234b962bf863c3053ce3a52368f6ba6))

# [15.5.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v15.4.8...v15.5.0) (2023-07-19)


### Bug Fixes

* 🐛 Add missing validation in Authorization Endpoint ([c525405](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/c5254050c9b3c5be4f3a1d5ee7ed01af4fcf59ee))


### Features

* 🎸 Entitlements able to take SAML released values ([df7e34b](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/df7e34b2cc54c852aa4005e6f894918eb17284c5))
* 🎸 Implementation of proxiedTokenIntrospection ([8513825](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/8513825c02e82d99bdb0585e862e37f097a4423e))

## [15.4.8](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v15.4.7...v15.4.8) (2023-07-17)


### Bug Fixes

* **deps:** update dependency org.springframework:spring-framework-bom to v5.3.29 ([7aded2c](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/7aded2c31989fd172de742544aa8e567a3d75cf7))

## [15.4.7](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v15.4.6...v15.4.7) (2023-07-17)


### Bug Fixes

* **deps:** update dependency com.google.guava:guava to v32.1.1-jre ([2e0296b](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/2e0296bcbe26ce4319572e959ff4fe3c70c7f961))

## [15.4.6](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v15.4.5...v15.4.6) (2023-06-22)


### Bug Fixes

* **deps:** update dependency org.springframework.security:spring-security-bom to v5.8.4 ([2509231](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/250923179363f049aea203b5634fce324fcd7191))

## [15.4.5](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v15.4.4...v15.4.5) (2023-06-20)


### Bug Fixes

* **deps:** update dependency io.sentry:sentry-bom to v6.23.0 ([f261a27](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/f261a272d6f66eb98ab78bdec0697fc52cc848f3))
* proper language switch URL on logout page ([b028ea3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/b028ea31ae99859fa0f2964293b6b0cf680698a6))

## [15.4.4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v15.4.3...v15.4.4) (2023-06-20)


### Bug Fixes

* update cesnet footer ([78e98ed](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/78e98ed93108203c76e54a459566ce4a120575d4))

## [15.4.3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v15.4.2...v15.4.3) (2023-06-18)


### Bug Fixes

* **deps:** update dependency org.springframework:spring-framework-bom to v5.3.28 ([525c39d](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/525c39d6db00f3500676970f2bb33a7b2580e0d9))

## [15.4.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v15.4.1...v15.4.2) (2023-06-16)


### Bug Fixes

* **deps:** update logback.version to v1.4.8 ([4c4c32e](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/4c4c32e2d0c5d455471ae14e3484ab308ec3537f))

## [15.4.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v15.4.0...v15.4.1) (2023-06-12)


### Bug Fixes

* **deps:** update dependency com.google.guava:guava to v32.0.1-jre ([f261ea8](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/f261ea8904922b5c134b7bdf91496578f01b07a7))

# [15.4.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v15.3.3...v15.4.0) (2023-06-07)


### Features

* 🎸 Forward original request URL in RelayState to IdP ([9b7ba91](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/9b7ba91587facbe63290c7a2097398258aea7a36))

## [15.3.3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v15.3.2...v15.3.3) (2023-06-03)


### Bug Fixes

* **deps:** update dependency com.fasterxml.jackson.dataformat:jackson-dataformat-yaml to v2.15.2 ([3812d46](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/3812d46598f7d169cba38875fafe1e44ab88e7af))

## [15.3.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v15.3.1...v15.3.2) (2023-05-31)


### Bug Fixes

* 🐛 Fix auth_time in Introspection endpoint ([6be299e](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/6be299ea9ed6d4277e18dbb8f0de3dcf919d97b7))

## [15.3.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v15.3.0...v15.3.1) (2023-05-30)


### Bug Fixes

* **deps:** update dependency com.google.guava:guava to v32 ([d2cad1e](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/d2cad1ec6a7eba45a28dc92287fbcf2752de714a))
* **deps:** update dependency io.sentry:sentry-bom to v6.20.0 ([f7e653f](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/f7e653f63d88535db089ae69b360f2804460c219))

# [15.3.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v15.2.5...v15.3.0) (2023-05-30)


### Features

* 🎸 Added AuthProcFilter to write statistics via API ([0e367bf](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/0e367bf12653ed1488ca94f61957f41d57ff1e8f))

## [15.2.5](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v15.2.4...v15.2.5) (2023-05-29)


### Bug Fixes

* 🐛 Fix AuthProc evaluation rules ([b320513](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/b320513dab0bc53ac54560421db14b9942a9f418))

## [15.2.4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v15.2.3...v15.2.4) (2023-05-29)


### Bug Fixes

* fix registration url attr name ([db29f44](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/db29f449c1db534182807fe41332ef3f40bdf342))
* print value in perunattributevalue tostring method ([4b46508](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/4b4650870c01df9df3e3efff300fc048e58fc80c))

## [15.2.3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v15.2.2...v15.2.3) (2023-05-28)


### Bug Fixes

* **deps:** update dependency org.projectlombok:lombok to v1.18.28 ([5e90184](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/5e90184e19250392dc4bb5747a323c4c35c0d8a9))

## [15.2.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v15.2.1...v15.2.2) (2023-05-20)


### Bug Fixes

* **deps:** update dependency com.fasterxml.jackson.dataformat:jackson-dataformat-yaml to v2.15.1 ([503ccdf](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/503ccdf8893d449d6baf3d24f9db86511e822c80))

## [15.2.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v15.2.0...v15.2.1) (2023-05-15)


### Bug Fixes

* remove 4kgateway ([4818fb9](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/4818fb9cbe217002529d7f3af2188f91bec9f409))

# [15.2.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v15.1.8...v15.2.0) (2023-05-05)


### Features

* 🎸 Stats filter - enable customized username attribute ([ad9b03d](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/ad9b03d9b06ad719fd4d1bbca83f5106a0c2d8c6))

## [15.1.8](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v15.1.7...v15.1.8) (2023-05-02)


### Bug Fixes

* 🐛 Fix computation of krb ticket expiration ([639ffee](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/639ffee75c8270cf8ee1505ddda2fc70afa554a7))
* 🐛 Fix expiresIn counting in GA4GHTokenExchange ([f817883](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/f817883ee84f5ab7178d2481b22783758331aaef))
* **deps:** update dependency com.fasterxml.jackson.dataformat:jackson-dataformat-yaml to v2.15.0 ([d736832](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/d736832abdb5fe80eec802c8cad2e3b994156ef8))
* **deps:** update dependency io.sentry:sentry-bom to v6.18.1 ([6f9b43a](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/6f9b43a9081cd645b5bf9d22456e1553bf95fc5f))

## [15.1.7](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v15.1.6...v15.1.7) (2023-05-02)


### Reverts

* revert "fix: skip OIDC logout confirmation page" ([16761ad](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/16761ad75a2486602cd147e183fe886825492dcd))

## [15.1.6](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v15.1.5...v15.1.6) (2023-05-01)


### Bug Fixes

* **deps:** update dependency org.mariadb.jdbc:mariadb-java-client to v3.1.4 ([152c2f3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/152c2f3f5454fcacc99b5a3a5760fd00d35674b2))

## [15.1.5](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v15.1.4...v15.1.5) (2023-04-22)


### Bug Fixes

* **deps:** update logback.version to v1.4.7 ([c9b7af8](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/c9b7af84bea428aced6ad27f12e145b7b586849d))

## [15.1.4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v15.1.3...v15.1.4) (2023-04-20)


### Bug Fixes

* **deps:** update dependency org.springframework.security:spring-security-bom to v5.8.3 ([793144a](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/793144a5b0368e0c2c4993a33f03fc4292b5abd6))

## [15.1.3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v15.1.2...v15.1.3) (2023-04-20)


### Bug Fixes

* **deps:** update dependency com.mysql:mysql-connector-j to v8.0.33 ([67d9e46](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/67d9e4684e7501dfb3a07b386070104b3892ac8b))

## [15.1.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v15.1.1...v15.1.2) (2023-04-18)


### Bug Fixes

* remove some unsupported items from provider metadata ([da544b8](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/da544b804427014d3fb66e6edf47fd89962bda1a))

## [15.1.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v15.1.0...v15.1.1) (2023-04-16)


### Bug Fixes

* **deps:** update dependency org.springframework:spring-framework-bom to v5.3.27 ([425eb7b](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/425eb7bdd6fb01f43b00aba6bdc503ba36dd8c62))

# [15.1.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v15.0.7...v15.1.0) (2023-04-13)


### Features

* 🎸 Kerberos token exchange ([a89c6b9](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/a89c6b97e43e7f2008d4cd6e6a031abdc7712327))

## [15.0.7](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v15.0.6...v15.0.7) (2023-04-11)


### Bug Fixes

* skip OIDC logout confirmation page ([7ef1269](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/7ef12699ae500d79446663df5e1f8e41a8d6ff19))

## [15.0.6](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v15.0.5...v15.0.6) (2023-04-11)


### Bug Fixes

* **deps:** update dependency com.nimbusds:nimbus-jose-jwt to v9.31 ([ea610ed](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/ea610ed1b90447dd1d49a7a3e1254c58a8f2a110))
* **deps:** update dependency io.sentry:sentry-bom to v6.17.0 ([6362f4b](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/6362f4b8ba619eb2e41c8d5d77f448443f106646))
* **deps:** update dependency org.postgresql:postgresql to v42.6.0 ([9990b8e](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/9990b8ea2e6a5e60eda006847a8feafc204fa4b4))

## [15.0.5](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v15.0.4...v15.0.5) (2023-04-09)


### Bug Fixes

* **deps:** update dependency org.apache.directory.api:api-all to v2.1.3 ([3a4ba72](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/3a4ba7274a15da57db49dd7bc629a49a94032293))

## [15.0.4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v15.0.3...v15.0.4) (2023-03-23)


### Bug Fixes

* **deps:** update dependency org.springframework:spring-framework-bom to v5.3.26 ([726ada4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/726ada40c8b13983d79417e0ba4af37f1f6acd7d))

## [15.0.3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v15.0.2...v15.0.3) (2023-03-22)


### Bug Fixes

* **deps:** update dependency org.mariadb.jdbc:mariadb-java-client to v3.1.3 ([449ccd6](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/449ccd6ed85344c84a5707983e5c1df43d5cb6a3))

## [15.0.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v15.0.1...v15.0.2) (2023-03-20)


### Bug Fixes

* **deps:** update dependency org.slf4j:slf4j-api to v2.0.7 ([7f6ce3b](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/7f6ce3be32513c724dfae6141c3ae609c1ce23cf))

## [15.0.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v15.0.0...v15.0.1) (2023-03-18)


### Bug Fixes

* **deps:** update logback.version to v1.4.6 ([cb6db17](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/cb6db1763a9be36dfdae276f2abe2228e09af1b2))

# [15.0.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v14.5.4...v15.0.0) (2023-03-01)


### Features

* id_token and access token claims defined by RFC 9068 ([0e29b98](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/0e29b9868d06cf5f4ede339e8347837c16ae24bf))


### BREAKING CHANGES

* value of typ in access token changes from JWT to at+jwt

## [14.5.4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v14.5.3...v14.5.4) (2023-03-01)


### Reverts

* fix case in at+jwt header of access_token ([883752c](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/883752c1b04db8ba52607db3b765dd052b104f3b))
* id_token and access_token missing claims (JWT profile) ([4ae0422](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/4ae0422d28775b884165658de545804cc3b060c5))

## [14.5.3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v14.5.2...v14.5.3) (2023-02-28)


### Bug Fixes

* 🐛 Fix case in at+jwt header of access_token ([170f3a8](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/170f3a870e7b634f2a26f00db2a454d07f04f15a))

## [14.5.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v14.5.1...v14.5.2) (2023-02-24)


### Bug Fixes

* **deps:** update dependency org.springframework.security:spring-security-bom to v5.8.2 ([38ab196](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/38ab196247c89e11415745b98a37eed3c8a9e968))

## [14.5.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v14.5.0...v14.5.1) (2023-02-24)


### Bug Fixes

* 🐛 Correctly return error responses for promt=none ([914c865](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/914c8653972a65727e57d157e3b95ff8a584fa4e))

# [14.5.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v14.4.6...v14.5.0) (2023-02-22)


### Features

* 🎸 IDtoken and access_token missing claims (JWT profile) ([a47c5cb](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/a47c5cbc374f7422292ed16f41d49eb3fa5cdd8b))

## [14.4.6](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v14.4.5...v14.4.6) (2023-02-20)


### Bug Fixes

* **deps:** update dependency io.sentry:sentry-bom to v6.14.0 ([248c942](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/248c942b8ff222ec403cbfa80a432f936148ebaa))

## [14.4.5](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v14.4.4...v14.4.5) (2023-02-20)


### Bug Fixes

* load spring security oauth2 schema from GitHub ([eace85f](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/eace85f235e90f1fba7db3f83d2e1a268fdeac1d))

## [14.4.4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v14.4.3...v14.4.4) (2023-02-19)


### Bug Fixes

* **deps:** update dependency org.postgresql:postgresql to v42.5.4 ([adfa844](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/adfa844a9b36a8dbe3490b88af1adcef2e8a8206))

## [14.4.3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v14.4.2...v14.4.3) (2023-02-18)


### Bug Fixes

* **deps:** update eclipse-persistence.version to v2.7.12 ([77117de](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/77117de423e61f4b35c65ea06d7eb37966000251))

## [14.4.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v14.4.1...v14.4.2) (2023-02-18)


### Bug Fixes

* **deps:** update dependency com.nimbusds:nimbus-jose-jwt to v9.30.2 ([9713070](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/9713070f25eb3a5f46184c20c3067ef671936c8d))

## [14.4.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v14.4.0...v14.4.1) (2023-02-14)


### Bug Fixes

* 🐛 Set Scoping in SAML based on requester-id prefix enabled ([8247a34](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/8247a343f5cb2855874a882a62d8fbc30e9780ec))

# [14.4.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v14.3.3...v14.4.0) (2023-02-13)


### Features

* 🎸 Make adding client_id to requesterID configurable ([e563d58](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/e563d58c21862371b4d00bec4332e54cbfa3a1b8))

## [14.3.3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v14.3.2...v14.3.3) (2023-02-10)


### Bug Fixes

* **deps:** update dependency io.sentry:sentry-bom to v6.13.1 ([dfe3fbc](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/dfe3fbc69b9b8d1b656286840fa55626dadbe418))

## [14.3.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v14.3.1...v14.3.2) (2023-02-06)


### Bug Fixes

* **deps:** update dependency org.postgresql:postgresql to v42.5.3 ([569a825](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/569a825b5b01808f5ae0733a1c70dee59fcc581b))

## [14.3.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v14.3.0...v14.3.1) (2023-02-06)


### Bug Fixes

* **deps:** update dependency org.glassfish.jaxb:jaxb-runtime to v2.3.8 ([18a8fcf](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/18a8fcf5cf3c73edb463503a6cab56dee6fc5b06))

# [14.3.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v14.2.10...v14.3.0) (2023-02-06)


### Features

* 🎸 Token exchange ([52a8eed](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/52a8eedd0a45d0a76392867e81e90726a6d07462))

## [14.2.10](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v14.2.9...v14.2.10) (2023-02-06)


### Bug Fixes

* **deps:** update dependency org.projectlombok:lombok to v1.18.26 ([88895ee](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/88895ee6b44ccd89b858d867471c72c4854052cf))

## [14.2.9](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v14.2.8...v14.2.9) (2023-02-04)


### Bug Fixes

* **deps:** update dependency org.postgresql:postgresql to v42.5.2 ([4315f2a](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/4315f2a912f091f96c743e7ae1402cfb2033d625))

## [14.2.8](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v14.2.7...v14.2.8) (2023-02-03)


### Bug Fixes

* **deps:** update dependency com.nimbusds:nimbus-jose-jwt to v9.30.1 ([c411598](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/c411598284df87670f3484d69c5d6813c75db6d7))

## [14.2.7](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v14.2.6...v14.2.7) (2023-02-03)


### Bug Fixes

* **deps:** update dependency com.nimbusds:nimbus-jose-jwt to v9.30 ([9cd8428](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/9cd84284c65f25c8399a5a9657fdc630a2106e7a))
* **deps:** update dependency io.sentry:sentry-bom to v6.13.0 ([0a2c71a](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/0a2c71a4c04e5d59462816919714da292d705a22))

## [14.2.6](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v14.2.5...v14.2.6) (2023-02-01)


### Bug Fixes

* **deps:** update dependency com.fasterxml.jackson.dataformat:jackson-dataformat-yaml to v2.14.2 ([e5bde47](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/e5bde47af291366fbc4f6116d864bbba5da71247))

## [14.2.5](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v14.2.4...v14.2.5) (2023-01-25)


### Bug Fixes

* 🐛 Fix tests after deps update ([a6ec354](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/a6ec354bba7b8c3980f1e4674fd167842ef5c665))

## [14.2.4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v14.2.3...v14.2.4) (2023-01-25)


### Bug Fixes

* **deps:** update dependency com.nimbusds:nimbus-jose-jwt to v9.29 ([fd49e81](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/fd49e81ec49d57edced14e6b7658223046f88793))

## [14.2.3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v14.2.2...v14.2.3) (2023-01-24)


### Bug Fixes

* **deps:** update dependency org.mariadb.jdbc:mariadb-java-client to v3.1.2 ([c2f91ee](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/c2f91ee62d41f9b313aaa5516f3534d8406fe5a6))

## [14.2.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v14.2.1...v14.2.2) (2023-01-21)


### Bug Fixes

* **deps:** update dependency com.mysql:mysql-connector-j to v8.0.32 ([f46fdbd](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/f46fdbd941561781cf82e44090b73f4877ccffe8))

## [14.2.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v14.2.0...v14.2.1) (2023-01-20)


### Bug Fixes

* 🐛 Coorectly process max_age=0 ([f31a72d](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/f31a72de6d941d5ed531b13948377d3b07dfd00d))

# [14.2.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v14.1.5...v14.2.0) (2023-01-20)


### Bug Fixes

* 🐛 Use authnInstant from SAML in auth_time ID token claim ([337600e](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/337600e017601e355363b1e0dd14bf9fe2b7dc29))


### Features

* 🎸 Pass client_id in SAML RequesterIds ([7ce4e1f](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/7ce4e1fba6a3d9f679db0e347c1d760e5a7b450e))

## [14.1.5](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v14.1.4...v14.1.5) (2023-01-20)


### Bug Fixes

* **deps:** update dependency io.sentry:sentry-bom to v6.12.1 ([3ed1b6b](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/3ed1b6ba2f254719a96a26c92bd8d4982ea89d5f))

## [14.1.4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v14.1.3...v14.1.4) (2023-01-19)


### Bug Fixes

* 🐛 Remove acrs comparison disabling ([266874c](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/266874c2f2ded14ba385fb962e83f3aedccac0b0))
* **deps:** update dependency io.sentry:sentry-bom to v6.12.0 ([b875b44](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/b875b44a23e848d547c7323fd6f71be8b8d041ec))

## [14.1.3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v14.1.2...v14.1.3) (2023-01-14)


### Bug Fixes

* **deps:** update dependency org.springframework:spring-framework-bom to v5.3.25 ([87c169f](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/87c169f2ab3052448eb2ccdded8503bb13b30e98))

## [14.1.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v14.1.1...v14.1.2) (2023-01-13)


### Bug Fixes

* **deps:** update dependency org.mariadb.jdbc:mariadb-java-client to v3.1.1 ([83d65db](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/83d65dbf7a084748501cb2b32a1fa02e972a1d04))

## [14.1.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v14.1.0...v14.1.1) (2023-01-09)


### Bug Fixes

* **deps:** update dependency com.google.code.gson:gson to v2.10.1 ([3e60d69](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/3e60d6983b3b292864f1540e5bd89b751d0bfbad))

# [14.1.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v14.0.11...v14.1.0) (2023-01-09)


### Bug Fixes

* **deps:** update dependency com.nimbusds:nimbus-jose-jwt to v9.28 ([fa8c306](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/fa8c30637eeff48df86cd01dcd1ff904dd489395))


### Features

* 🎸 Move devicecode params to internal processing ([6a29cad](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/6a29cad1395a61bd63d24ceefe91098fe4f748d0))

## [14.0.11](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v14.0.10...v14.0.11) (2023-01-05)


### Bug Fixes

* 🐛 Fix error in unapproved_spec.jsp ([47e7cda](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/47e7cda1f73a6b7799fa6a394c5e672d30637a5e))

## [14.0.10](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v14.0.9...v14.0.10) (2023-01-03)


### Bug Fixes

* **deps:** update dependency io.sentry:sentry-bom to v6.11.0 ([b8ab892](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/b8ab89266f4e777f86103b5ff86c41158aa963ee))

## [14.0.9](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v14.0.8...v14.0.9) (2023-01-02)


### Bug Fixes

* **deps:** update dependency com.nimbusds:nimbus-jose-jwt to v9.26 ([2b2e2f1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/2b2e2f176771a715077d803d23ee538e1c5163a9))
* **deps:** update shedlock monorepo to v4.44.0 ([45dba01](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/45dba017c5aa4a39d36b78c3db1e6bbca00801cf))

## [14.0.8](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v14.0.7...v14.0.8) (2022-12-24)


### Bug Fixes

* **deps:** update dependency org.aspectj:aspectjweaver to v1.9.19 ([43405a3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/43405a3aab02e5f0a0315c0db52d1dffe5e0e535))
* **deps:** update dependency org.springframework.security:spring-security-bom to v5.8.1 ([d98150c](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/d98150c9ea179c44fc33217f6b7732273dcae459))

## [14.0.7](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v14.0.6...v14.0.7) (2022-12-21)


### Bug Fixes

* **deps:** update dependency io.sentry:sentry-bom to v6.10.0 ([a562a3d](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/a562a3dbf269a939be1965566be8460e0050385a))

## [14.0.6](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v14.0.5...v14.0.6) (2022-12-16)


### Bug Fixes

* **deps:** update shedlock monorepo to v4.43.0 ([1ec60ed](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/1ec60ed9272ea4debf849bf3313c7991b08d95d6))

## [14.0.5](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v14.0.4...v14.0.5) (2022-12-16)


### Bug Fixes

* **deps:** update dependency io.sentry:sentry-bom to v6.9.2 ([8c2fbe8](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/8c2fbe888f4c14ca600ed5a216fe986fe7f93a98))

## [14.0.4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v14.0.3...v14.0.4) (2022-12-15)


### Bug Fixes

* **deps:** update dependency org.slf4j:slf4j-api to v2.0.6 ([8c6a4b0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/8c6a4b0a27e7396d19b085b30ae1ac5781a45487))

## [14.0.3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v14.0.2...v14.0.3) (2022-12-14)


### Bug Fixes

* 🐛 Remove illegal import in LS AAI Approve device view ([c69ad37](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/c69ad37a1467a5fb32c2ac7765704b3cc9f58471))

## [14.0.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v14.0.1...v14.0.2) (2022-12-13)


### Bug Fixes

* 🐛 Reworked how Jurisdiction for LS AAI is processed ([6e47761](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/6e4776153c5cf4ff3e2dad67743c3778d54203f7))

## [14.0.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v14.0.0...v14.0.1) (2022-12-07)


### Bug Fixes

* 🐛 enable e-INFRA CZ footer date range copyright ([2bf2fd9](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/2bf2fd91d254c6774b737df20bd31b4c533c11e3))
* 🐛 Fix using pageContext.* to request.* in views ([c44775b](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/c44775b438a6769a0b74b042a45c2f553c3cd4b4))
* 🐛 Remove unnecessary c:set in JSPs ([007327b](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/007327bca3dc940f03a0945d473028100d33fd9f))

# [14.0.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v13.0.13...v14.0.0) (2022-12-06)


### Features

* 🎸 GA4GH Passport via token exchange implemented ([c5d32fd](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/c5d32fd076ca907c0571716438f37a76e89c2669))


### BREAKING CHANGES

* 🧨 Removed obsolete ClaimSource classes for GA4GH Passports

## [13.0.13](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v13.0.12...v13.0.13) (2022-12-04)


### Bug Fixes

* **deps:** update dependency org.apache.httpcomponents:httpclient to v4.5.14 ([59b0cc0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/59b0cc021cb35661c5539eb28bb82f0c02e88c0f))

## [13.0.12](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v13.0.11...v13.0.12) (2022-12-02)


### Bug Fixes

* 🐛 Fix processing of prompt param in device_code flow ([fb40e2f](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/fb40e2f5aa4a3da5852a4870d185455687e033da))

## [13.0.11](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v13.0.10...v13.0.11) (2022-12-02)


### Bug Fixes

* 🐛 Fix loading scripts, URL attrs in tags, resource mapping ([6dc3c05](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/6dc3c05f37035f2f851d46e050d80ef229ccdfbe))
* 🐛 Remove EuroPDX template ([114a5fa](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/114a5fa104f1a8365a2fe0130b3b8d7b65e1370a))
* **deps:** update dependency org.springframework.security:spring-security-bom to v5.8.0 ([2a4e90f](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/2a4e90f9a94e987ec57903a269aebec67b059c14))

## [13.0.10](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v13.0.9...v13.0.10) (2022-11-28)


### Bug Fixes

* **deps:** update dependency org.slf4j:slf4j-api to v2.0.5 ([397042d](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/397042dfee9f6d4a15ffecdea13f0eef72c37032))

## [13.0.9](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v13.0.8...v13.0.9) (2022-11-26)


### Bug Fixes

* **deps:** update dependency org.postgresql:postgresql to v42.5.1 ([ac64b1d](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/ac64b1d7f856df245e68a98272edc70f06f3842d))

## [13.0.8](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v13.0.7...v13.0.8) (2022-11-25)


### Bug Fixes

* **deps:** update dependency com.fasterxml.jackson.dataformat:jackson-dataformat-yaml to v2.14.1 ([7d26d93](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/7d26d93b401fc9eef13a25e721b79c6b3c2c1fc7))

## [13.0.7](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v13.0.6...v13.0.7) (2022-11-21)


### Bug Fixes

* **deps:** update logback.version to v1.4.5 ([9bde46f](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/9bde46f556a8395bc839bcc683bde782b24c07b1))

## [13.0.6](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v13.0.5...v13.0.6) (2022-11-20)


### Bug Fixes

* **deps:** update dependency org.slf4j:slf4j-api to v2.0.4 ([06ae5c6](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/06ae5c6be28e7379261d9e0e8dd236f3532d19c0))

## [13.0.5](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v13.0.4...v13.0.5) (2022-11-19)


### Bug Fixes

* **deps:** update dependency io.sentry:sentry-bom to v6.7.1 ([6308520](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/6308520f3eac1045e63eec23c9a826f16acfdafd))

## [13.0.4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v13.0.3...v13.0.4) (2022-11-19)


### Bug Fixes

* **deps:** update dependency org.springframework:spring-framework-bom to v5.3.24 ([a4a9e47](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/a4a9e476dceb92fd1c4f0052b1873af8ddbf21c3))

## [13.0.3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v13.0.2...v13.0.3) (2022-11-16)


### Bug Fixes

* **deps:** update dependency com.fasterxml.jackson.dataformat:jackson-dataformat-yaml to v2.14.0 ([0f1bb48](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/0f1bb48cce81ce0fccb2ee434e89052b319a5206))
* **deps:** update dependency com.google.code.gson:gson to v2.10 ([e55a079](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/e55a079cb26f4147fc8d141565b99d4fa8d08a00))
* **deps:** update dependency io.sentry:sentry-bom to v6.7.0 ([9a0f7b8](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/9a0f7b8ed6fec283c89fc6f87eceeaa974298df1))
* **deps:** update dependency org.mariadb.jdbc:mariadb-java-client to v3.1.0 ([c210d28](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/c210d2801b1ee6b12dd926cc975e67991ae99eab))

## [13.0.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v13.0.1...v13.0.2) (2022-11-10)


### Bug Fixes

* **deps:** update dependency org.mariadb.jdbc:mariadb-java-client to v3.0.9 ([1543a47](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/1543a47e206f1fbaebcd133bfe168429fea4d03e))

## [13.0.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v13.0.0...v13.0.1) (2022-11-03)


### Bug Fixes

* **deps:** update dependency org.springframework.security:spring-security-bom to v5.7.5 ([5b16b5f](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/5b16b5f2fda172d0f5089ee788daeaf59f2918b5))

# [13.0.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v12.1.7...v13.0.0) (2022-11-03)


### Documentation

* default configuration changed ([d196788](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/d1967886923f265e2df34c7e2f4df2aa838be782))


### BREAKING CHANGES

* default values of some build parameters changed

## [12.1.7](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v12.1.6...v12.1.7) (2022-10-21)


### Bug Fixes

* **deps:** update dependency org.glassfish.jaxb:jaxb-runtime to v2.3.7 ([04d9eca](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/04d9eca5396633e4b0906a1ad0e6624aee3be1b4))

## [12.1.6](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v12.1.5...v12.1.6) (2022-10-20)


### Bug Fixes

* **deps:** update dependency org.springframework.security:spring-security-bom to v5.7.4 ([74303da](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/74303da13e34784d107935c7fb29d373829e23ad))

## [12.1.5](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v12.1.4...v12.1.5) (2022-10-19)


### Bug Fixes

* 🐛 Fix missing audience in introspection result and tokens ([066ab03](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/066ab03f24242ab2b7a5675e44028298de3f2da0))

## [12.1.4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v12.1.3...v12.1.4) (2022-10-18)


### Bug Fixes

* **deps:** update dependency mysql:mysql-connector-java to v8.0.31 ([f186ec5](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/f186ec58ab93f59baafe0e76d555456c581a6ca5))

## [12.1.3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v12.1.2...v12.1.3) (2022-10-16)


### Bug Fixes

* **deps:** update dependency com.nimbusds:nimbus-jose-jwt to v9.25.6 ([69fb008](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/69fb008c6cb26e27ab42038d85cfe082bd7c037b))

## [12.1.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v12.1.1...v12.1.2) (2022-10-13)


### Bug Fixes

* **deps:** update dependency io.sentry:sentry-bom to v6.4.4 ([908f540](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/908f540b04cc0586f742c163dac11507fbfe760a))

## [12.1.1](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v12.1.0...v12.1.1) (2022-10-13)


### Bug Fixes

* **deps:** update logback.version to v1.4.4 ([f70c258](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/f70c258c9877fd8edd49a5799a5b9c0efdcf051f))

# [12.1.0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v12.0.4...v12.1.0) (2022-10-11)


### Bug Fixes

* 🐛 Cleanup SavedUserAuth orphan records via scheduler ([9ba9166](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/9ba9166e6365b84695476dd716bde5418b1ba888))
* 🐛 Correctly process prompt=none ([1762840](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/176284003985f56245a3514e28f65d82c787196e))
* 🐛 Small change in pom.xml to trigger fix release ([599d6d0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/599d6d0515209d75c9b7c49b1418597a0d78a763))
* **deps:** update dependency com.nimbusds:nimbus-jose-jwt to v9.25.4 ([087556a](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/087556a44defc44d996af574c1170a7bc5879de4))
* **deps:** update dependency io.sentry:sentry-bom to v6.4.2 ([74de154](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/74de1547862f0177bd7e3787ba42e2b7fca239b6))
* **deps:** update dependency io.sentry:sentry-bom to v6.4.3 ([5c332e7](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/5c332e761148a9fb22f4738644a489fee4c6e716))
* **deps:** update dependency org.mariadb.jdbc:mariadb-java-client to v3.0.8 ([a4ce8ba](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/a4ce8ba0dcf953fcc1de27559224cb3a768fe50e))
* **deps:** update dependency org.slf4j:slf4j-api to v2.0.3 ([f272320](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/f272320189ce6a04df943adf1513305b6a8f2115))
* **deps:** update dependency org.springframework:spring-framework-bom to v5.3.23 ([1bfa9e4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/1bfa9e46c28d7cee71dcf291e4a0d35a4ccc9a56))
* **deps:** update logback.version to v1.4.1 ([1ab70a5](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/1ab70a5025cc6d71c9b56e0c7d30deb38c901a69))
* **deps:** update logback.version to v1.4.3 ([5bd1b84](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/5bd1b8494467e273e408d74da8d2f5f0e31fe2d8))
* **deps:** update shedlock.version to v4.42.0 ([4479d55](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/4479d5522b41bb3a42ee6626123724cd91ddba68))


### Features

* trigger semantic release ([a021c64](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/a021c64fb751890df6dd35f46f0f8fcc2b35526c))

## [12.0.5](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v12.0.4...v12.0.5) (2022-10-11)


### Bug Fixes

* 🐛 Cleanup SavedUserAuth orphan records via scheduler ([9ba9166](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/9ba9166e6365b84695476dd716bde5418b1ba888))
* 🐛 Correctly process prompt=none ([1762840](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/176284003985f56245a3514e28f65d82c787196e))
* 🐛 Small change in pom.xml to trigger fix release ([599d6d0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/599d6d0515209d75c9b7c49b1418597a0d78a763))
* **deps:** update dependency com.nimbusds:nimbus-jose-jwt to v9.25.4 ([087556a](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/087556a44defc44d996af574c1170a7bc5879de4))
* **deps:** update dependency io.sentry:sentry-bom to v6.4.2 ([74de154](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/74de1547862f0177bd7e3787ba42e2b7fca239b6))
* **deps:** update dependency io.sentry:sentry-bom to v6.4.3 ([5c332e7](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/5c332e761148a9fb22f4738644a489fee4c6e716))
* **deps:** update dependency org.mariadb.jdbc:mariadb-java-client to v3.0.8 ([a4ce8ba](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/a4ce8ba0dcf953fcc1de27559224cb3a768fe50e))
* **deps:** update dependency org.slf4j:slf4j-api to v2.0.3 ([f272320](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/f272320189ce6a04df943adf1513305b6a8f2115))
* **deps:** update dependency org.springframework:spring-framework-bom to v5.3.23 ([1bfa9e4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/1bfa9e46c28d7cee71dcf291e4a0d35a4ccc9a56))
* **deps:** update logback.version to v1.4.1 ([1ab70a5](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/1ab70a5025cc6d71c9b56e0c7d30deb38c901a69))
* **deps:** update logback.version to v1.4.3 ([5bd1b84](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/5bd1b8494467e273e408d74da8d2f5f0e31fe2d8))
* **deps:** update shedlock.version to v4.42.0 ([4479d55](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/4479d5522b41bb3a42ee6626123724cd91ddba68))

## [12.0.5](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v12.0.4...v12.0.5) (2022-10-09)


### Bug Fixes

* 🐛 Cleanup SavedUserAuth orphan records via scheduler ([9ba9166](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/9ba9166e6365b84695476dd716bde5418b1ba888))
* **deps:** update dependency com.nimbusds:nimbus-jose-jwt to v9.25.4 ([087556a](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/087556a44defc44d996af574c1170a7bc5879de4))
* **deps:** update dependency io.sentry:sentry-bom to v6.4.2 ([74de154](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/74de1547862f0177bd7e3787ba42e2b7fca239b6))
* **deps:** update dependency io.sentry:sentry-bom to v6.4.3 ([5c332e7](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/5c332e761148a9fb22f4738644a489fee4c6e716))
* **deps:** update dependency org.mariadb.jdbc:mariadb-java-client to v3.0.8 ([a4ce8ba](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/a4ce8ba0dcf953fcc1de27559224cb3a768fe50e))
* **deps:** update dependency org.slf4j:slf4j-api to v2.0.3 ([f272320](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/f272320189ce6a04df943adf1513305b6a8f2115))
* **deps:** update dependency org.springframework:spring-framework-bom to v5.3.23 ([1bfa9e4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/1bfa9e46c28d7cee71dcf291e4a0d35a4ccc9a56))
* **deps:** update logback.version to v1.4.1 ([1ab70a5](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/1ab70a5025cc6d71c9b56e0c7d30deb38c901a69))
* **deps:** update logback.version to v1.4.3 ([5bd1b84](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/5bd1b8494467e273e408d74da8d2f5f0e31fe2d8))
* **deps:** update shedlock.version to v4.42.0 ([4479d55](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/4479d5522b41bb3a42ee6626123724cd91ddba68))

## [12.0.5](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v12.0.4...v12.0.5) (2022-10-06)


### Bug Fixes

* 🐛 Cleanup SavedUserAuth orphan records via scheduler ([9ba9166](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/9ba9166e6365b84695476dd716bde5418b1ba888))
* **deps:** update dependency com.nimbusds:nimbus-jose-jwt to v9.25.4 ([087556a](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/087556a44defc44d996af574c1170a7bc5879de4))
* **deps:** update dependency io.sentry:sentry-bom to v6.4.2 ([74de154](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/74de1547862f0177bd7e3787ba42e2b7fca239b6))
* **deps:** update dependency org.mariadb.jdbc:mariadb-java-client to v3.0.8 ([a4ce8ba](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/a4ce8ba0dcf953fcc1de27559224cb3a768fe50e))
* **deps:** update dependency org.slf4j:slf4j-api to v2.0.3 ([f272320](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/f272320189ce6a04df943adf1513305b6a8f2115))
* **deps:** update dependency org.springframework:spring-framework-bom to v5.3.23 ([1bfa9e4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/1bfa9e46c28d7cee71dcf291e4a0d35a4ccc9a56))
* **deps:** update logback.version to v1.4.1 ([1ab70a5](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/1ab70a5025cc6d71c9b56e0c7d30deb38c901a69))
* **deps:** update logback.version to v1.4.3 ([5bd1b84](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/5bd1b8494467e273e408d74da8d2f5f0e31fe2d8))
* **deps:** update shedlock.version to v4.42.0 ([4479d55](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/4479d5522b41bb3a42ee6626123724cd91ddba68))

## [12.0.5](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v12.0.4...v12.0.5) (2022-10-06)


### Bug Fixes

* 🐛 Cleanup SavedUserAuth orphan records via scheduler ([9ba9166](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/9ba9166e6365b84695476dd716bde5418b1ba888))
* **deps:** update dependency com.nimbusds:nimbus-jose-jwt to v9.25.4 ([087556a](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/087556a44defc44d996af574c1170a7bc5879de4))
* **deps:** update dependency io.sentry:sentry-bom to v6.4.2 ([74de154](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/74de1547862f0177bd7e3787ba42e2b7fca239b6))
* **deps:** update dependency org.mariadb.jdbc:mariadb-java-client to v3.0.8 ([a4ce8ba](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/a4ce8ba0dcf953fcc1de27559224cb3a768fe50e))
* **deps:** update dependency org.slf4j:slf4j-api to v2.0.3 ([f272320](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/f272320189ce6a04df943adf1513305b6a8f2115))
* **deps:** update dependency org.springframework:spring-framework-bom to v5.3.23 ([1bfa9e4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/1bfa9e46c28d7cee71dcf291e4a0d35a4ccc9a56))
* **deps:** update logback.version to v1.4.1 ([1ab70a5](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/1ab70a5025cc6d71c9b56e0c7d30deb38c901a69))
* **deps:** update shedlock.version to v4.42.0 ([4479d55](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/4479d5522b41bb3a42ee6626123724cd91ddba68))

## [12.0.4](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v12.0.3...v12.0.4) (2022-10-05)


### Bug Fixes

* **deps:** update dependency com.nimbusds:nimbus-jose-jwt to v9.25.4 ([6c63d1b](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/6c63d1bb58705fbad6a72f63ee1ccb40459a9284))
* **deps:** update dependency io.sentry:sentry-bom to v6.4.2 ([e5f6ff0](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/e5f6ff051a83c368287fb7082e4568a2e6eeeaf9))
* **deps:** update dependency org.mariadb.jdbc:mariadb-java-client to v3.0.8 ([3adc42f](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/3adc42fb7370a975b87f45d90a75a43e104f4d8f))
* **deps:** update dependency org.springframework:spring-framework-bom to v5.3.23 ([599a31a](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/599a31a17f105d9a78c2065cc06cd3b811387466))

## [12.0.3](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v12.0.2...v12.0.3) (2022-10-04)


### Bug Fixes

* update link in copyright notice ([b0c5561](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/b0c5561f6ef9eb4591d586e0fbc4302dc19919f3))

## [12.0.2](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/compare/v12.0.1...v12.0.2) (2022-09-28)


### Bug Fixes

* first release in GitLab ([9a1f20b](https://gitlab.ics.muni.cz/perun/perun-proxyidp/v1/OpenID-Connect-Java-Spring-Server/commit/9a1f20be079bd6fe48087c4ea56cda28e4e00a45))

## [12.0.1](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v12.0.0...v12.0.1) (2022-09-19)


### Bug Fixes

* 🐛 Remove forceAuthn for MFA ([e3ff40c](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/e3ff40c91be1e3f74247009b5a4aee8fcd2ed3bd))

# [12.0.0](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v11.0.3...v12.0.0) (2022-09-13)


### Features

* 🎸 better introspectionr results ([ee1dda8](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/ee1dda8c84e69604621d6bd39ded1dac5e13b6fa))


### BREAKING CHANGES

* Requires db update (see v12.0.0.sql)

## [11.0.3](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v11.0.2...v11.0.3) (2022-09-13)


### Bug Fixes

* **deps:** update dependency com.nimbusds:nimbus-jose-jwt to v9.25 ([d90579c](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/d90579cfd98cd28fc8ee494d67d08596fd795184))

## [11.0.2](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v11.0.1...v11.0.2) (2022-09-13)


### Bug Fixes

* **deps:** update dependency com.fasterxml.jackson.dataformat:jackson-dataformat-yaml to v2.13.4 ([8a4de44](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/8a4de4438b1bbffc76a60dedf3f61e1165451bd7))

## [11.0.1](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v11.0.0...v11.0.1) (2022-09-13)


### Bug Fixes

* 🐛 Fix non-existing continue_direct view name ([b0813df](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/b0813df4ffd13465c5c29fe7dccdc915279d17b7))

# [11.0.0](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v10.5.4...v11.0.0) (2022-09-13)


### Features

* 🎸 Return samlError from token in devicecode ([ff184f1](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/ff184f1bfd19875564783456bf84e98cde1c329d))


### BREAKING CHANGES

* requires DB update

## [10.5.4](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v10.5.3...v10.5.4) (2022-09-13)


### Bug Fixes

* **deps:** update dependency io.sentry:sentry-bom to v6.4.1 ([14d76c1](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/14d76c1adc6ae5b2f5295e30494064c2f40a80bb))

## [10.5.3](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v10.5.2...v10.5.3) (2022-09-13)


### Bug Fixes

* 🐛 Downgrade logback to preserve compatibility ([97b2a0d](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/97b2a0d8ac4610994500cfc1eda3d6ca818f492d))

## [10.5.2](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v10.5.1...v10.5.2) (2022-09-13)


### Bug Fixes

* **deps:** update logback.version to v1.4.0 ([7849045](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/78490450eb417166c4856238989369ca3ca20bca))

## [10.5.1](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v10.5.0...v10.5.1) (2022-08-29)


### Bug Fixes

* **deps:** update dependency com.nimbusds:nimbus-jose-jwt to v9.24.3 ([fa7e1e0](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/fa7e1e0ddb4b65495b0080c784e197bfe54ce95c))
* **deps:** update dependency io.sentry:sentry-bom to v6.4.0 ([20caf2a](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/20caf2ac243d7b458e77c146c4852ea35685af2b))
* **deps:** update dependency org.postgresql:postgresql to v42.5.0 ([e534f7b](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/e534f7b0c8b884aa18e64d08ac522e9061361cf2))

# [10.5.0](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v10.4.0...v10.5.0) (2022-08-26)


### Features

* return error response on noAuthnContext ([7d1f731](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/7d1f73104e123eb5b0ad87eb37bc0eb46b0e65bd))

# [10.4.0](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v10.3.4...v10.4.0) (2022-08-26)


### Features

* Integration with sentry ([219f31c](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/219f31c3fccd71f4f0754c5e2552a5d16148096a))

## [10.3.4](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v10.3.3...v10.3.4) (2022-08-24)


### Bug Fixes

* 🐛 Allow calling /devicecode without client secret ([02d8d34](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/02d8d34fb1f6dae8ba29371768fa4c5a20338a0d))

## [10.3.3](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v10.3.2...v10.3.3) (2022-08-22)


### Bug Fixes

* **deps:** update dependency org.postgresql:postgresql to v42.4.2 ([8fce861](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/8fce861d7dd4e93786cdd2778c39ef72753eb3bf))
* **deps:** update shedlock.version to v4.41.0 ([2b60811](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/2b60811a5ac7c85ce6809932d0b69786285cbf27))

## [10.3.2](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v10.3.1...v10.3.2) (2022-08-20)


### Bug Fixes

* **deps:** update dependency com.nimbusds:nimbus-jose-jwt to v9.24.2 ([5bdccc7](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/5bdccc7a644975191db5ec7e4c568ef386dbff5b))

## [10.3.1](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v10.3.0...v10.3.1) (2022-08-19)


### Bug Fixes

* **deps:** update dependency org.apache.directory.api:api-all to v2.1.2 ([61f49e6](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/61f49e661037bfdde833afdb40c5385046db3e13))
* **deps:** update dependency org.springframework.security:spring-security-bom to v5.7.3 ([530bdb2](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/530bdb262ef5c892dff30125610e7fb67c3ebe33))

# [10.3.0](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v10.2.1...v10.3.0) (2022-08-16)


### Features

* GA4GH ClaimSource by API call ([0753598](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/075359886e6c56b1ff5e3cefc2e1b12d381a4e38))

## [10.2.1](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v10.2.0...v10.2.1) (2022-08-15)


### Bug Fixes

* **deps:** update shedlock.version to v4.40.0 ([c597037](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/c597037ec98b03e034a84c55665079e73ce474cc))

# [10.2.0](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v10.1.3...v10.2.0) (2022-08-15)


### Features

* 🎸 Spring5 & Spring-security 5 ([3faa9a6](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/3faa9a68ba64c93558644d31e80f42c39cc38fd5))

## [10.1.3](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v10.1.2...v10.1.3) (2022-08-15)


### Bug Fixes

* **deps:** update eclipse-persistence.version to v2.7.11 ([d85ea1c](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/d85ea1cb9600ec484c4347cc77737763e6de253d))

## [10.1.2](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v10.1.1...v10.1.2) (2022-08-08)


### Bug Fixes

* **deps:** update dependency org.apache.directory.api:api-all to v2.1.1 ([741e502](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/741e5027cc82d8fc282f716f760baf7cb5414fa3))

## [10.1.1](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v10.1.0...v10.1.1) (2022-08-08)


### Bug Fixes

* **deps:** update dependency com.google.code.gson:gson to v2.9.1 ([9b42b50](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/9b42b50cc92d775eab13d3cbd8719d9ac16cf6dc))
* **deps:** update dependency org.mariadb.jdbc:mariadb-java-client to v3.0.7 ([c27a5c5](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/c27a5c58bc8428c97a57f50292a640455807c34d))

# [10.1.0](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v10.0.3...v10.1.0) (2022-07-28)


### Features

* 🎸 Configurable timeouts in RPC connector ([a929858](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/a929858026de152e8c33c2819f551302b292a443))

## [10.0.3](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v10.0.2...v10.0.3) (2022-07-27)


### Bug Fixes

* correct postgreSQL for v10 breaking change ([28a9411](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/28a9411af439608ebe43b5b332cf4bb29569c652))
* **deps:** update dependency mysql:mysql-connector-java to v8.0.30 ([5426aa9](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/5426aa9835463c748506004664e24791c8d76d67))

## [10.0.2](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v10.0.1...v10.0.2) (2022-07-25)


### Bug Fixes

* 🐛 Fix nullPointerexception in AuthProcFilterInit ([64c0d51](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/64c0d510597382f3257ed98424fdc239b6e33fd6))

## [10.0.1](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v10.0.0...v10.0.1) (2022-07-13)


### Bug Fixes

* 🐛 Fix script loading for LS footer ([bd90a76](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/bd90a763888d91e1b29449612503b8e2ce9b93d1))

# [10.0.0](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v9.4.0...v10.0.0) (2022-07-09)


### Bug Fixes

* 🐛 Fix displaying for consent for EMBL ([ef47df1](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/ef47df163f76a10e8d504b4068c20d1204e7d048))


### BREAKING CHANGES

* 🧨 DB changes (see v10.0.0.sql files)

# [9.4.0](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v9.3.2...v9.4.0) (2022-07-08)


### Features

* IsEligible authproc filter and claim source ([2e0aaa7](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/2e0aaa772bb063260b6b4abbf3919b01c7320df9))

## [9.3.2](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v9.3.1...v9.3.2) (2022-07-04)


### Bug Fixes

* **deps:** update dependency org.mariadb.jdbc:mariadb-java-client to v3.0.6 ([fc02c8f](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/fc02c8f79c9b034ab8f2eff929f183f52bb120ed))

## [9.3.1](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v9.3.0...v9.3.1) (2022-06-15)


### Bug Fixes

* **deps:** update dependency org.postgresql:postgresql to v42.4.0 ([9f56413](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/9f56413f05854b9a093ca4c324d5429a8a13fbfe))

# [9.3.0](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v9.2.2...v9.3.0) (2022-06-03)


### Features

* 🎸Claim sources for extracting AuthenticationContextClassRef and AuthnInstant ([d9d3034](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/d9d3034e552676353db30eb3066b56e0d78c6bfc))

## [9.2.2](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v9.2.1...v9.2.2) (2022-06-03)


### Bug Fixes

* 🐛 Fix SAML Claim source when singleValue to use joiner ([d16c3c6](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/d16c3c6368a4039ac6918f8a68960bfaac899dab))
* 🐛 Fixed displaying consent screens for LS template ([9884eb1](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/9884eb1f0ee1a400b8d6ca285390801ed12086e9))

## [9.2.1](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v9.2.0...v9.2.1) (2022-06-01)


### Bug Fixes

* **deps:** update dependency com.nimbusds:nimbus-jose-jwt to v9.23 ([0c465ca](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/0c465ca1ff2ef4bbfd331cf35532a6d9fd30cf96))

# [9.2.0](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v9.1.6...v9.2.0) (2022-05-30)


### Bug Fixes

* **deps:** update dependency org.mariadb.jdbc:mariadb-java-client to v3.0.5 ([e6a8342](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/e6a834243896443f8495af1dab9a560ef8ba4d6e))
* **deps:** update dependency org.postgresql:postgresql to v42.3.6 ([c1d62ca](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/c1d62ca98ec137898e7363713493c28f2d44b496))


### Features

* Added new claims sources ([15cf3a9](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/15cf3a95eb0f62e4fc4be7f2a1e791683ca189cb))

## [9.1.6](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v9.1.5...v9.1.6) (2022-05-23)


### Bug Fixes

* **deps:** update dependency com.fasterxml.jackson.dataformat:jackson-dataformat-yaml to v2.13.3 ([e5f3a62](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/e5f3a629807630aba823ca313a7573b2cc8010ec))

## [9.1.5](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v9.1.4...v9.1.5) (2022-05-09)


### Bug Fixes

* **deps:** update dependency org.postgresql:postgresql to v42.3.5 ([319d0c7](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/319d0c7c2a190c20889768668b6d702cab796cea))

## [9.1.4](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v9.1.3...v9.1.4) (2022-04-25)


### Bug Fixes

* **deps:** update dependency com.nimbusds:nimbus-jose-jwt to v9.22 ([1a087e4](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/1a087e4ba1da9ef89d5315a09bd41e4acc591c21))

## [9.1.3](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v9.1.2...v9.1.3) (2022-04-25)


### Bug Fixes

* **deps:** update dependency org.springframework.security.oauth:spring-security-oauth2 to v2.5.2.release ([5eafd46](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/5eafd46d0a53a2bdb88b0cb2086e0fface4022be))

## [9.1.2](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v9.1.1...v9.1.2) (2022-04-25)


### Bug Fixes

* **deps:** update dependency mysql:mysql-connector-java to v8.0.29 ([9ff89f7](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/9ff89f78f8571e7becd8d9391b788ee035298a57))

## [9.1.1](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v9.1.0...v9.1.1) (2022-04-22)


### Bug Fixes

* 🐛 Fixed wrong mail in LS consent ([c84912c](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/c84912c55106b5b272e5221efbacdbd1451aeb7e))

# [9.1.0](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v9.0.3...v9.1.0) (2022-04-22)


### Features

* 🎸 Filter for logging authentication details ([585dbd8](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/585dbd82a5364e5ca9fe16a9b5714aa340f47896))

## [9.0.3](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v9.0.2...v9.0.3) (2022-04-22)


### Bug Fixes

* **deps:** update dependency org.projectlombok:lombok to v1.18.24 ([6736cf4](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/6736cf4c38adad8b37e8c9914348da938c69506c))
* improve MUNI header ([3f0f910](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/3f0f9103d8223ff41476f04e6c6a034a483da84f))

## [9.0.2](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v9.0.1...v9.0.2) (2022-04-20)


### Bug Fixes

* MUNI branding ([07479e4](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/07479e4a04fdbbee2a14c22739957d43d12e49c2))

## [9.0.1](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v9.0.0...v9.0.1) (2022-04-19)


### Bug Fixes

* **deps:** update dependency org.postgresql:postgresql to v42.3.4 ([cae6002](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/cae60026d32814e814ac76c8dd1a5b867a358e77))

# [9.0.0](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v8.2.5...v9.0.0) (2022-04-13)


### Features

* LS AAI design ([cd1ce6f](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/cd1ce6fcc2706d77f01c655b969b597c7f692f49))


### BREAKING CHANGES

* requires database update (see migraiton script),
dropped ELIXIR theme

## [8.2.5](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v8.2.4...v8.2.5) (2022-04-11)


### Bug Fixes

* show unapproved message ([0d6e2c7](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/0d6e2c70d8b9bc34f0e1a77a9af996966c72f2e5))

## [8.2.4](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v8.2.3...v8.2.4) (2022-04-11)


### Bug Fixes

* 🐛 Added missing return values when RPC disabled ([733597a](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/733597a4731dc840520672f344f55a290237e988))

## [8.2.3](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v8.2.2...v8.2.3) (2022-04-11)


### Bug Fixes

* 🐛 Fix nullPointer in SamlAuthenticationDetailsStringCon ([3c034f4](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/3c034f4c54965aa44fa654daf90520e5aa3f6a46))

## [8.2.2](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v8.2.1...v8.2.2) (2022-04-06)


### Bug Fixes

* 🐛 Fix storing SavedUserAuth ([c83ecc2](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/c83ecc28e20ed44da21b7c3b9172f0cda5a50d12))

## [8.2.1](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v8.2.0...v8.2.1) (2022-04-04)


### Bug Fixes

* 🐛 Remove RelayState from SAML details in SavedUserAuth ([0f73d88](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/0f73d882363d1d5adb2bcf464f55339614947ff2))

# [8.2.0](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v8.1.9...v8.2.0) (2022-04-04)


### Features

* 🎸 More user lookup methods ([3ea2b82](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/3ea2b82053651a331c015b774ce69107f679ecd9))

## [8.1.9](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v8.1.8...v8.1.9) (2022-04-04)


### Bug Fixes

* **deps:** update dependency org.aspectj:aspectjweaver to v1.9.9.1 ([fb56956](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/fb5695632497b257955a9d72a8d2a83cda65b5a8))

## [8.1.8](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v8.1.7...v8.1.8) (2022-03-31)


### Bug Fixes

* **deps:** update dependency org.aspectj:aspectjweaver to v1.9.9 ([4ef0063](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/4ef006308a23111f9270b6d3c6b86d6c1f0f174f))
* **deps:** update dependency org.mariadb.jdbc:mariadb-java-client to v3.0.4 ([96358d9](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/96358d989b46618a048e71fdce310ce11ac807ce))

## [8.1.7](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v8.1.6...v8.1.7) (2022-03-23)


### Bug Fixes

* **deps:** update eclipse-persistence.version to v2.7.10 ([2f864fc](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/2f864fca1cdfe2affec8602f8525f30a26d63565))

## [8.1.6](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v8.1.5...v8.1.6) (2022-03-23)


### Bug Fixes

* 🐛 Allow Group description to be empty string ([76899b4](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/76899b44777160ec8c12c682af4e940388a72c60))
* **deps:** update dependency com.fasterxml.jackson.dataformat:jackson-dataformat-yaml to v2.13.2 ([1db9d51](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/1db9d5113a629f893817c08385909729b2819580))

## [8.1.5](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v8.1.4...v8.1.5) (2022-03-09)


### Bug Fixes

* **deps:** update dependency com.nimbusds:nimbus-jose-jwt to v9.21 ([b1810d8](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/b1810d82baff39ba912fbb8619c2e4fd5ff027e4))

## [8.1.4](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v8.1.3...v8.1.4) (2022-03-09)


### Bug Fixes

* **deps:** update logback.version to v1.2.11 ([8601f9c](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/8601f9c8724135a5d89f7eed2726bceaec898246))

## [8.1.3](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v8.1.2...v8.1.3) (2022-03-09)


### Bug Fixes

* **deps:** update dependency com.google.guava:guava to v31.1-jre ([1032ed0](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/1032ed065ac5e45d51f3dd8520e535b97a43ec63))

## [8.1.2](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v8.1.1...v8.1.2) (2022-02-17)


### Bug Fixes

* 🐛 Fix missing execute statement in statistics filter ([93b8081](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/93b8081c330419d339a3c7a520df8047c453b578))

## [8.1.1](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v8.1.0...v8.1.1) (2022-02-17)


### Bug Fixes

* **deps:** update dependency com.google.code.gson:gson to v2.9.0 ([0ec65b6](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/0ec65b6eeed09fbe5a1c77a17b0e0e278b8a3354))

# [8.1.0](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v8.0.6...v8.1.0) (2022-02-17)


### Bug Fixes

* **deps:** update dependency com.nimbusds:nimbus-jose-jwt to v9.19 ([bb1443f](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/bb1443fd9e838895061543b3376580fbee7acaf0))
* **deps:** update dependency org.aspectj:aspectjweaver to v1.9.8 ([78087dc](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/78087dc71621c49d0081b812b8c3bd193907ef1b))
* **deps:** update dependency org.postgresql:postgresql to v42.3.3 ([9810e84](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/9810e84915abe429cacf5e91f431115c19cb9e6c))


### Features

* 🎸 Display noAuthnContext message on login_failure ([8872469](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/8872469c197639ef445878ba34b61ee754f06bad))

## [8.0.6](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v8.0.5...v8.0.6) (2022-02-01)


### Bug Fixes

* **deps:** update dependency com.nimbusds:nimbus-jose-jwt to v9.18 ([6653cdb](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/6653cdbfe0f9cc073fc62b4d5eca5e9d5778400c))

## [8.0.5](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v8.0.4...v8.0.5) (2022-02-01)


### Bug Fixes

* **deps:** update dependency org.mariadb.jdbc:mariadb-java-client to v3 ([b3ddb12](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/b3ddb12e8daa6f46f8d64bed709775037f4c58c8))

## [8.0.4](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v8.0.3...v8.0.4) (2022-02-01)


### Bug Fixes

* **deps:** update dependency org.glassfish.jaxb:jaxb-runtime to v2.3.6 ([64f8997](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/64f899708f13d9b23f1eac65cdae5dba3e71dd24))

## [8.0.3](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v8.0.2...v8.0.3) (2022-01-26)


### Bug Fixes

* 🐛 Consider empty referer as external ([d4bc19e](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/d4bc19e2d8e8a9750c71ad8065fd26a97704da80))

## [8.0.2](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v8.0.1...v8.0.2) (2022-01-13)


### Bug Fixes

* 🐛 Set email verified to true ([93fc557](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/93fc5577f57d7102c2c29f4fdd6087751a60e60b))

## [8.0.1](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v8.0.0...v8.0.1) (2022-01-12)


### Bug Fixes

* 🐛 Fix missing sub in ClaimSourceProduceContext ([5eace9f](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/5eace9fb21fce78e8e05e1b4eba8e47143f88c49))

# [8.0.0](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v7.5.2...v8.0.0) (2022-01-12)


### Features

* 🎸 Refactored userinfo serv., new SAML-based claim sources ([2c413d9](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/2c413d9916e8a862d91a3be93490bed832245c70))


### BREAKING CHANGES

* 🧨 requires database update

## [7.5.2](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v7.5.1...v7.5.2) (2022-01-10)


### Bug Fixes

* 🐛 Do not display remember me when prompt=consnet ([1bf72b8](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/1bf72b802ade1f04e35c55061615895ccb435c48))

## [7.5.1](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v7.5.0...v7.5.1) (2021-12-23)


### Bug Fixes

* incorrect label on stay logged in button ([75a626f](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/75a626f9daa0a58fc58f03307574b09a5ac17849))

# [7.5.0](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v7.4.1...v7.5.0) (2021-12-10)


### Features

* 🎸 Configurable favicons ([bf227df](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/bf227df26e364a61c3ba08d122e8bccdbcc9184c))

## [7.4.1](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v7.4.0...v7.4.1) (2021-12-09)


### Bug Fixes

* 🐛 Fix inserting and reading properties in the stats filter ([31710bf](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/31710bf5f5b14009904ec38c88e7a8e80a8d9d8d))

# [7.4.0](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v7.3.0...v7.4.0) (2021-12-09)


### Features

* 🎸 Configurable name of user col in stats filter ([4a5be5d](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/4a5be5d32baf754d6550747cba30fb1a8ec355fb))

# [7.3.0](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v7.2.0...v7.3.0) (2021-12-09)


### Features

* 🎸 Configurable max pool size for JDBC data sources ([e5b406e](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/e5b406e85311166a6e9c54ec0d4d52637557746b))

# [7.2.0](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v7.1.1...v7.2.0) (2021-12-08)


### Features

* 🎸 Added automated bundling of .war file into release ([cd1118f](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/cd1118f1a0c8121fd49fe73b70b6074ea1ce4a0c))

## [7.1.1](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v7.1.0...v7.1.1) (2021-12-08)


### Bug Fixes

* 🐛 Fix possible SQL exceptions ([b3bd9e9](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/b3bd9e94c76b7781a31d362fe8fda61242b30d83))

# [7.1.0](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v7.0.0...v7.1.0) (2021-12-07)


### Bug Fixes

* DB inserts in statistics work with PostgreSQL ([b72eb8f](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/b72eb8fd8e4633205c86e1861f6acb7558ac62de))


### Features

* Added configurable ipdIdColumnName and spIdColumnName in statistics ([515f99b](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/515f99b25518d6e8be66a0c50133c01216c0bde5))

# [7.0.0](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v6.0.0...v7.0.0) (2021-12-06)


### Code Refactoring

* 💡 Refactored GA4GH Passports and visas ([a94fd99](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/a94fd992dd5889745b93b25e2d17460569688c16))


### Features

* 🎸 Implemented BBMRI-ERIC Ga4gh Passports and Visas ([141e6c8](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/141e6c8653112e1b3b0beda2ea3ba8be3eca4bca))


### BREAKING CHANGES

* 🧨 Ga4gh Claim source class for ELIXIR has been changed. Also, the
ElixirAccessTokenModifier class has been moved and renamed.

# [6.0.0](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v5.0.1...v6.0.0) (2021-12-06)


### Code Refactoring

* 💡 Drop support for java 8 ([4a0b63e](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/4a0b63ec0e67c519cd9c0af79c8224777761090b))


### BREAKING CHANGES

* 🧨 Dropped support for java 8

## [5.0.1](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v5.0.0...v5.0.1) (2021-12-02)


### Bug Fixes

* 🐛 Fix fallbacking of locale to the code to prevent errors ([ceb01c7](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/ceb01c78e760e32e803be30c559e772323bd68cb))

# [5.0.0](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v4.0.1...v5.0.0) (2021-11-30)


### Code Refactoring

* 💡 Refactored how translations are loaded and used ([665b45f](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/665b45fb419a7dedc20de62ec0e1c6d550b7f3bd))


### BREAKING CHANGES

* Property `web.langs.customfiles.path` must point to the
RersourceBundle.

## [4.0.1](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v4.0.0...v4.0.1) (2021-11-19)


### Bug Fixes

* 🐛 Fixed missing ACRs code and device_code flows ([4d3b072](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/4d3b07225c1f7b1abb7a9c79d170326fa81c2aa8))

# [4.0.0](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v3.5.0...v4.0.0) (2021-11-19)


### Bug Fixes

* 🐛 Fix ACR for implicit and authorization_code flows ([39bc00a](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/39bc00a3b08e3129e2244f123a466f4c9490ae36))


### BREAKING CHANGES

* 🧨 Database needs to be updated: `ALTER TABLE saved_user_auth DROP
source_class; ALTER TABLE saved_user_auth ADD COLUMN acr VARCHAR(1024);`

# [3.5.0](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v3.4.1...v3.5.0) (2021-11-16)


### Features

* 🎸 AARC_IDP_HINTING implemented ([ebd1459](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/ebd1459ba3eac20717c80955c5dbc725fd3934f8))

## [3.4.1](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v3.4.0...v3.4.1) (2021-11-15)


### Bug Fixes

* 🐛 Added missing PostgreSQL dependency ([e12c164](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/e12c164b46cbf9efb1a3516cb8c03e307e7049c2))

# [3.4.0](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v3.3.0...v3.4.0) (2021-11-12)


### Features

* 🎸 Forward client_id in AuthenticationContextClass ([6a6d1e3](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/6a6d1e3ad92d3c6785f0e786aaf4c3fa5f04b806))

# [3.3.0](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v3.2.0...v3.3.0) (2021-11-11)


### Features

* 🎸 Extended list of internal referrers for sess. invalider ([9aa16ff](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/9aa16ffe5cb1c1b045d9f1f71cd94751d9d876b4))
* 🎸 Make SAML identifier attribute configurable ([3949857](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/39498573c3d62284298bae0df48fbbcf071e9caf))

# [3.2.0](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v3.1.0...v3.2.0) (2021-11-09)


### Features

* 🎸 Adderd e-INFRA CZ template ([5eb50f6](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/5eb50f64414db6a42cff76003c5b41f4e8e03535))

# [3.1.0](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v3.0.1...v3.1.0) (2021-11-08)


### Features

* 🎸 Sign refresh tokens ([23a6354](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/23a6354fc708bd89301bf2cac0619bbebb431f4f))

## [3.0.1](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/compare/v3.0.0...v3.0.1) (2021-11-05)


### Bug Fixes

* 🐛 fix loading JWKS ([371adc1](https://github.com/CESNET/OpenID-Connect-Java-Spring-Server/commit/371adc13fbff6150a32fcd8b5242ef03899c758b))

Unreleased:

*1.3.3*:
- Authorization codes are now longer
- Client/RS can parse the "sub" and "user_id" claims in introspection response
- Database-direct queries for fetching tokens by user (optimization)
- Device flow supports verification_uri_complete (must be turned on)
- Long scopes display properly and are still checkable
- Language system remebers when it can't find a file and stops throwing so many errors
- Index added for refresh tokens
- Updated to Spring Security 4.2.11
- Updated Spring to 4.3.22
- Change approve pages to use issuer instead of page context
- Updated oracle database scripts

*1.3.2*:
- Added changelog
- Set default redirect URI resolver strict matching to true
- Fixed XSS vulnerability on redirect URI display on approval page
- Removed MITRE from copyright
- Disallow unsigned JWTs on client authentication
- Upgraded Nimbus revision
- Added French translation
- Added hooks for custom JWT claims
- Removed "Not Yet Implemented" tag from post-logout redirect URI

*1.3.1*:
- Added End Session endpoint
- Fixed discovery endpoint
- Downgrade MySQL connector dependency version from developer preview to GA release

*1.3.0*:
- Added device flow support
- Added PKCE support
- Modularized UI to allow better overlay and extensions
- Modularized data import/export API
- Added software statements to dynamic client registration
- Added assertion processing framework
- Removed ID tokens from storage
- Removed structured scopes

*1.2.6*: 
- Added strict HEART compliance mode
