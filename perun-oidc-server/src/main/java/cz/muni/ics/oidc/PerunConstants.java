package cz.muni.ics.oidc;

public interface PerunConstants {

    String REGISTRAR_TARGET_NEW = "targetnew";
    String REGISTRAR_TARGET_EXISTING = "targetexisting";
    String REGISTRAR_TARGET_EXTENDED = "targetextended";

    String REGISTRAR_PARAM_VO = "vo";
    String REGISTRAR_PARAM_GROUP = "group";

    String GROUP_NAME_MEMBERS = "members";
}
