package cz.muni.ics.oidc.saml;

import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import static cz.muni.ics.oidc.server.filters.AuthProcFilterConstants.PROMPT_LOGIN;
import static cz.muni.ics.oidc.server.filters.AuthProcFilterConstants.PROMPT_SELECT_ACCOUNT;

@Slf4j
public class PerunSamlUtils {

    public static boolean needsReAuthByPrompt(String prompt) {
        if (prompt == null) {
            return false;
        }
        boolean res = (StringUtils.hasText(prompt) && (PROMPT_LOGIN.equalsIgnoreCase(prompt)
            || PROMPT_SELECT_ACCOUNT.equalsIgnoreCase(prompt)));
        log.debug("requires reAuth by prompt - {}", res);
        return res;
    }

}
