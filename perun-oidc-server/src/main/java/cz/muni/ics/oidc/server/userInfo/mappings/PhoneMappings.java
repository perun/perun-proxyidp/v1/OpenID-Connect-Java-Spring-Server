package cz.muni.ics.oidc.server.userInfo.mappings;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@ToString
public class PhoneMappings {

    private String phoneNumber = null;
    private String phoneNumberVerified = null;

    public Set<String> getAttrNames() {
        return new HashSet<>(Arrays.asList(phoneNumber, phoneNumberVerified));
    }

}
