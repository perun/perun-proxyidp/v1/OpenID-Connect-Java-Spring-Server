package cz.muni.ics.oauth2.model.proxied_introspection.auth;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;

import java.util.HashMap;
import java.util.Map;

/**
 * Supported authentication methods to remote introspection endpoint.
 *
 * @see cz.muni.ics.oauth2.model.proxied_introspection.ProxiedIntrospectionEndpoint
 *
 * @author Dominik Frantisek Bucik <bucik@ics.muni.cz>
 */
@Getter
@AllArgsConstructor
public enum ProxiedIntrospectionAuthMethod {

    SECRET_POST("client_secret_post"),
    SECRET_BASIC("client_secret_basic"),
    NONE("none");

    @NonNull
    private final String value;

    // map to aid reverse lookup
    private static final Map<String, ProxiedIntrospectionAuthMethod> lookup = new HashMap<>();
    static {
        for (ProxiedIntrospectionAuthMethod a : ProxiedIntrospectionAuthMethod.values()) {
            lookup.put(a.getValue(), a);
        }
    }

    public static ProxiedIntrospectionAuthMethod getByValue(String value) {
        return lookup.get(value);
    }

}
