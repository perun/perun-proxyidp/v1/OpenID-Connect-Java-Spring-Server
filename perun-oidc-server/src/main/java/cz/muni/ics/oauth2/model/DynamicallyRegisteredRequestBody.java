package cz.muni.ics.oauth2.model;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class DynamicallyRegisteredRequestBody {

    @JsonAlias("client_name")
    private String clientName;

    @JsonAlias("client_description")
    private String clientDescription;

    @JsonAlias("redirect_uris")
    private Set<String> redirectUris = new HashSet<>();

    @JsonAlias("client_uri")
    private String clientUri;

    @JsonAlias("contacts")
    private Set<String> contacts = new HashSet<>();

    @JsonAlias("tos_uri")
    private String tosUri;

    @JsonAlias("token_endpoint_auth_method")
    private String tokenEndpointAuthMethod = "client_secret_basic";

    @JsonAlias("scope")
    private Set<String> scope = new HashSet<>();

    @JsonAlias("grant_types")
    private Set<String> grantTypes = new HashSet<>();

    @JsonAlias("response_types")
    private Set<String> responseTypes = new HashSet<>();

    @JsonAlias("policy_uri")
    private String policyUri;

    @JsonAlias("jwks_uri")
    private String jwksUri;

    @JsonAlias("jwks")
    private String jwks;

    @JsonAlias("software_id")
    private String softwareId;

    @JsonAlias("software_version")
    private String softwareVersion;

    @JsonAlias("application_type")
    private String applicationType;

    @JsonAlias("sector_identifier_uri")
    private String sectorIdentifierUri;

    @JsonAlias("subject_type")
    private String subjectType;

    @JsonAlias("request_object_signing_alg")
    private String requestObjectSigningAlg = null;

    @JsonAlias("userinfo_signed_response_alg")
    private String userInfoSignedResponseAlg = null;

    @JsonAlias("userinfo_encrypted_response_alg")
    private String userInfoEncryptedResponseAlg = null;

    @JsonAlias("userinfo_encrypted_response_enc")
    private String userInfoEncryptedResponseEnc = null;

    @JsonAlias("id_token_signed_response_alg")
    private String idTokenSignedResponseAlg = null;

    @JsonAlias("id_token_encrypted_response_alg")
    private String idTokenEncryptedResponseAlg = null;

    @JsonAlias("id_token_encrypted_response_enc")
    private String idTokenEncryptedResponseEnc = null;

    @JsonAlias("token_endpoint_auth_signing_alg")
    private String tokenEndpointAuthSigningAlg = null;

    @JsonAlias("default_max_age")
    private Integer defaultMaxAge;

    @JsonAlias("require_auth_time")
    private Boolean requireAuthTime;

    @JsonAlias("default_acr_values")
    private Set<String> defaultACRvalues;

    @JsonAlias("initiate_login_uri")
    private String initiateLoginUri;

    @JsonAlias("post_logout_redirect_uris")
    private Set<String> postLogoutRedirectUris = new HashSet<>();

    @JsonAlias("request_uris")
    private Set<String> requestUris = new HashSet<>();

    @JsonAlias("access_token_validity_seconds")
    private Integer accessTokenValiditySeconds = 3600;

    @JsonAlias("refresh_token_validity_seconds")
    private Integer refreshTokenValiditySeconds = 28800;

    @JsonAlias("resources")
    private Set<String> resource = new HashSet<>();

    @JsonAlias("reuse_refresh_token")
    private boolean reuseRefreshToken = false;

    @JsonAlias("id_token_validity_seconds")
    private Integer idTokenValiditySeconds = 3600;

    @JsonAlias("clear_access_tokens_on_refresh")
    private boolean clearAccessTokensOnRefresh = true;

    @JsonAlias("device_code_validity_seconds")
    private Integer deviceCodeValiditySeconds = 300;

    @JsonAlias("claim_redirect_uris")
    private Set<String> claimsRedirectUris = new HashSet<>();

    @JsonAlias("software_statement")
    private String softwareStatement;

    @JsonAlias("code_challenge_method")
    private String codeChallengeMethod;

}
