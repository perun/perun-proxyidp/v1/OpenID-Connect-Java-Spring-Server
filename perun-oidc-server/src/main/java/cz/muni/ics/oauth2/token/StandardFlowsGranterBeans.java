package cz.muni.ics.oauth2.token;

import cz.muni.ics.openid.connect.request.OAuth2RequestFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.TokenGranter;
import org.springframework.security.oauth2.provider.client.ClientCredentialsTokenGranter;
import org.springframework.security.oauth2.provider.code.AuthorizationCodeServices;
import org.springframework.security.oauth2.provider.implicit.ImplicitTokenGranter;
import org.springframework.security.oauth2.provider.refresh.RefreshTokenGranter;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;

@Configuration
public class StandardFlowsGranterBeans {

    @Bean
    @Autowired
    public TokenGranter authorizationCodeTokenGranter(AuthorizationServerTokenServices authorizationServerTokenServices,
                                                      AuthorizationCodeServices authorizationCodeServices,
                                                      ClientDetailsService clientDetailsService,
                                                      OAuth2RequestFactory oAuth2RequestFactory)
    {
        return new org.springframework.security.oauth2.provider.code.AuthorizationCodeTokenGranter(
                authorizationServerTokenServices, authorizationCodeServices, clientDetailsService, oAuth2RequestFactory
        );
    }

    @Bean
    @Autowired
    public TokenGranter refreshTokenGranter(AuthorizationServerTokenServices authorizationServerTokenServices,
                                            ClientDetailsService clientDetailsService,
                                            OAuth2RequestFactory oAuth2RequestFactory)
    {
        return new RefreshTokenGranter(authorizationServerTokenServices, clientDetailsService, oAuth2RequestFactory);
    }

    @Bean
    @Autowired
    public TokenGranter implicitTokenGranter(AuthorizationServerTokenServices authorizationServerTokenServices,
                                             ClientDetailsService clientDetailsService,
                                             OAuth2RequestFactory oAuth2RequestFactory)
    {
        return new ImplicitTokenGranter(authorizationServerTokenServices, clientDetailsService, oAuth2RequestFactory);
    }

    @Bean
    @Autowired
    public TokenGranter clientCredentialsTokenGranter(AuthorizationServerTokenServices authorizationServerTokenServices,
                                                      ClientDetailsService clientDetailsService,
                                                      OAuth2RequestFactory oAuth2RequestFactory)
    {
        ClientCredentialsTokenGranter tokenGranter = new ClientCredentialsTokenGranter(
                authorizationServerTokenServices, clientDetailsService, oAuth2RequestFactory);
        tokenGranter.setAllowRefresh(true);
        return tokenGranter;
    }

}
