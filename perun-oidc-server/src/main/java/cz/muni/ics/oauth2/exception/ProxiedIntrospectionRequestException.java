package cz.muni.ics.oauth2.exception;

public class ProxiedIntrospectionRequestException extends Exception {

    public ProxiedIntrospectionRequestException() {
        super();
    }

    public ProxiedIntrospectionRequestException(String message) {
        super(message);
    }

    public ProxiedIntrospectionRequestException(String message, Throwable cause) {
        super(message, cause);
    }

    public ProxiedIntrospectionRequestException(Throwable cause) {
        super(cause);
    }

    protected ProxiedIntrospectionRequestException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
