package cz.muni.ics.oauth2.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
public class AuthenticationStatement {

    private List<String> authenticatingAuthorities;
    private String authnContextClassRef;
    private String authnInstant;

}
