package cz.muni.ics.oauth2.model.proxied_introspection.auth;

import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpResponse;

import java.io.IOException;

/**
 * No-op authentication to remote introspection endpoint.
 *
 * @see cz.muni.ics.oauth2.model.proxied_introspection.ProxiedIntrospectionEndpoint
 *
 * @author Dominik Frantisek Bucik <bucik@ics.muni.cz>
 */
public class NoAuthProxiedIntrospectionAuthMethod implements ProxiedIntrospectionEndpointAuthMethod {

    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
            throws IOException
    {
        return execution.execute(request, body);
    }

}
