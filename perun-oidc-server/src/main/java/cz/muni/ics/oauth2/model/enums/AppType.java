package cz.muni.ics.oauth2.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

@Getter
@AllArgsConstructor
public enum AppType {
    WEB("web"), NATIVE("native");

    private final String value;

    // map to aid reverse lookup
    private static final Map<String, AppType> lookup = new HashMap<>();
    static {
        for (AppType a : AppType.values()) {
            lookup.put(a.getValue(), a);
        }
    }

    public static AppType getByValue(String value) {
        if (!isSupported(value)) {
            throw new IllegalArgumentException("Unrecognized value: " + value);
        }
        value = value.toLowerCase();
        return lookup.get(value);
    }

    public static boolean isSupported(String appType) {
        if (appType == null) {
            throw new IllegalArgumentException("appType must not be null");
        }
        appType = appType.toLowerCase();
        return lookup.containsKey(appType);
    }

}
