package cz.muni.ics.oauth2.model.proxied_introspection.auth;

import org.springframework.http.client.ClientHttpRequestInterceptor;

public interface ProxiedIntrospectionEndpointAuthMethod extends ClientHttpRequestInterceptor {

}
