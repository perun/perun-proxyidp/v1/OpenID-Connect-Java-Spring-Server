package cz.muni.ics.oauth2.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public class DynamicallyRegisteredRequestBodyResponse extends DynamicallyRegisteredRequestBody {

    private String clientId;

    private String clientSecret;

    private String registrationAccessToken;
    
    private String registrationClientUri;

    private Long clientIdIssuedAt;

    private Long clientSecretExpiresAt;

    public DynamicallyRegisteredRequestBodyResponse(
            ClientDetailsEntity registeredClient,
            String registrationAccessToken,
            String registrationClientUri,
            Long clientIdIssuedAt,
            Long clientSecretExpiresAt
    ) {
        super();
        this.setClientId(registeredClient.getClientId());
        this.setClientSecret(registeredClient.getClientSecret());
        this.setRegistrationAccessToken(registrationAccessToken);
        this.setRegistrationClientUri(registrationClientUri);
        this.setClientIdIssuedAt(clientIdIssuedAt);
        this.setClientSecretExpiresAt(clientSecretExpiresAt);
        mapFromClient(registeredClient);
    }

    private void mapFromClient(ClientDetailsEntity registeredClient) {
        this.setClientName(registeredClient.getClientName());
        this.setClientDescription(registeredClient.getClientDescription());
        this.setRedirectUris(registeredClient.getRedirectUris());
        this.setClientUri(registeredClient.getClientUri());
        this.setContacts(registeredClient.getContacts());
        this.setTosUri(registeredClient.getTosUri());
        if (registeredClient.getTokenEndpointAuthMethod() != null) {
            this.setTokenEndpointAuthMethod(registeredClient.getTokenEndpointAuthMethod().getValue());
        } else {
            this.setTokenEndpointAuthMethod(null);
        }
        this.setClientSecret(clientSecret);
        this.setScope(registeredClient.getScope());
        this.setGrantTypes(registeredClient.getGrantTypes());
        this.setResponseTypes(registeredClient.getResponseTypes());
        this.setPolicyUri(registeredClient.getPolicyUri());
        this.setJwksUri(registeredClient.getJwksUri());
        if (null != registeredClient.getJwks()) {
            this.setJwks(registeredClient.getJwks().toString());
        }
        this.setSoftwareId(registeredClient.getSoftwareId());
        this.setSoftwareVersion(registeredClient.getSoftwareVersion());
        if (null != registeredClient.getApplicationType()) {
            this.setApplicationType(registeredClient.getApplicationType().getValue());
        }
        this.setSectorIdentifierUri(registeredClient.getSectorIdentifierUri());
        if (null != registeredClient.getSubjectType()) {
            this.setSubjectType(registeredClient.getSubjectType().getValue());
        }
        if (null != registeredClient.getRequestObjectSigningAlg()) {
            this.setRequestObjectSigningAlg(registeredClient.getRequestObjectSigningAlg().getName());
        }
        if (null != registeredClient.getUserInfoSignedResponseAlg()) {
            this.setUserInfoSignedResponseAlg(registeredClient.getUserInfoSignedResponseAlg().getName());
        }
        if (null != registeredClient.getUserInfoEncryptedResponseAlg()) {
            this.setUserInfoEncryptedResponseAlg(registeredClient.getUserInfoEncryptedResponseAlg().getName());
        }
        if (null != registeredClient.getUserInfoEncryptedResponseEnc()) {
            this.setUserInfoEncryptedResponseEnc(registeredClient.getUserInfoEncryptedResponseEnc().getName());
        }
        if (null != registeredClient.getIdTokenSignedResponseAlg()) {
            this.setIdTokenSignedResponseAlg(registeredClient.getIdTokenSignedResponseAlg().getName());
        }
        if (null != registeredClient.getIdTokenEncryptedResponseAlg()) {
            this.setIdTokenEncryptedResponseAlg(registeredClient.getIdTokenEncryptedResponseAlg().getName());
        }
        if (null != registeredClient.getIdTokenEncryptedResponseEnc()) {
            this.setIdTokenEncryptedResponseEnc(registeredClient.getIdTokenEncryptedResponseEnc().getName());
        }
        if (null != registeredClient.getTokenEndpointAuthSigningAlg()) {
            this.setTokenEndpointAuthSigningAlg(registeredClient.getTokenEndpointAuthSigningAlg().getName());
        }
        this.setDefaultMaxAge(registeredClient.getDefaultMaxAge());
        this.setRequireAuthTime(registeredClient.getRequireAuthTime());
        this.setDefaultACRvalues(registeredClient.getDefaultACRvalues());
        this.setInitiateLoginUri(registeredClient.getInitiateLoginUri());
        this.setPostLogoutRedirectUris(registeredClient.getPostLogoutRedirectUris());
        this.setRequestUris(registeredClient.getRequestUris());
        this.setAccessTokenValiditySeconds(registeredClient.getAccessTokenValiditySeconds());
        this.setRefreshTokenValiditySeconds(registeredClient.getRefreshTokenValiditySeconds());
        this.setResource(registeredClient.getResourceIds());
        this.setReuseRefreshToken(registeredClient.isReuseRefreshToken());
        this.setIdTokenValiditySeconds(registeredClient.getIdTokenValiditySeconds());
        this.setClearAccessTokensOnRefresh(registeredClient.isClearAccessTokensOnRefresh());
        this.setDeviceCodeValiditySeconds(registeredClient.getDeviceCodeValiditySeconds());
        this.setClaimsRedirectUris(registeredClient.getClaimsRedirectUris());
        if (null != registeredClient.getSoftwareStatement()) {
            this.setSoftwareStatement(registeredClient.getSoftwareStatement().serialize());
        }

        if (null != registeredClient.getCodeChallengeMethod()) {
            this.setCodeChallengeMethod(registeredClient.getCodeChallengeMethod().getAlgorithm().getName());
        }
    }

}
