package cz.muni.ics.oauth2.service;

import cz.muni.ics.oauth2.model.ClientDetailsEntity;
import cz.muni.ics.oauth2.model.DynamicallyRegisteredRequestBody;

public interface DynamicClientRegistrationService {

    ClientDetailsEntity saveClient(String tokenClientId, DynamicallyRegisteredRequestBody requestedRegistration);

    void removeClient(String tokenClientId, String dynregClientId);

}
